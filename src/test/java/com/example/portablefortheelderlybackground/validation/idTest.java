package com.example.portablefortheelderlybackground.validation;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = idTest.class)
public class idTest {
    @Test
    public void idLengthValidation() {
        String s = String.valueOf(1659848961134956546L);
        System.out.println(s.length());
    }
}
