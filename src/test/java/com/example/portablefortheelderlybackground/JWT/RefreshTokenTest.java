package com.example.portablefortheelderlybackground.JWT;

import cn.hutool.json.JSONUtil;
import com.example.portablefortheelderlybackground.pojo.userDetailImpl;
import com.example.portablefortheelderlybackground.utils.JWTUtil;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(classes = RefreshTokenTest.class)
public class RefreshTokenTest {
    @Test
    public void test() {
        JWTUtil jwtUtil = new JWTUtil();
        String refreshToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdWJqZWN0IiwiY3JlYXRlZCI6MTY5NTk2Mzg0NzkzMSwiZXhwIjoxNjk3MTczNDQ3LCJ1c2VyRGV0YWlscyI6eyJ1c2VyIjp7ImlkIjoiMTY5ODY3ODgzNDA5OTk0OTU3MCIsIm5hbWUiOiLkvZnmoqblkJsiLCJhZ2UiOjE5LCJzZXgiOiLlpbMiLCJwaG9uZSI6IjE4Nzc5MzUxNTg2IiwicGFzc3dvcmQiOiIkMmEkMTAkMWM0cllHYmFvUlMyUVlkYjh2T0xLZUp6emtpMzl3bWs5YTl4MHhFZVd5TlZaQ1I4LzNnVEsiLCJhdmF0YXJVcmwiOiJodHRwczovL29zcy50emZjdy50b3AvRnFxMlJaWkNTY05mSTJaZURFYzdmS09ocFlFRCJ9L" +
                "CJwZXJtaXNzaW9uIjpbInN5c3RlbTpxdWVyeSIsInN5c3RlbTpkZWxldGUiLCJzeXN0ZW06aW5zZXJ0Iiwic3lzdGVtOnVwZGF0ZSIsInN5c3RlbTpvdXQiXSwiZW5hYmxlZCI6dHJ1ZSwiYWNjb3VudE5vbkxvY2tlZCI6dHJ1ZSwidXNlcm5hbWUiOiLkvZnmoqblkJsiLCJwYXNzd29yZCI6IiQyYSQxMCQxYzRyWUdiYW9SUzJRWWRiOHZPTEtlSnp6a2kzOXdtazlhOXgweEVlV3lOVlpDUjgvM2dUSyIsImNyZWRlbnRpYWxzTm9uRXhwaXJlZCI6dHJ1ZSwiYWNjb3VudE5vbkV4cGlyZWQiOnRydW" +
                "V9LCJ1c2VybmFtZSI6IuS9meaipuWQmyJ9.V6pm-lTvgeb4eJW8QKkUZQh73Hmqb3MqjdRQvKeEzsE";
        Jws<Claims> token = Jwts.parser()
                .setSigningKey("token")
                .parseClaimsJws(refreshToken);
        Claims body = token.getBody();
        String jsonStr = JSONUtil.toJsonStr(body.get("userDetails"));
        System.out.println(jsonStr);
        userDetailImpl userDetail = JSONUtil.toBean(jsonStr, userDetailImpl.class);
        System.out.println(userDetail);
    }
}
