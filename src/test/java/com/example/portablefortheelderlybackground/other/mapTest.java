package com.example.portablefortheelderlybackground.other;

import com.example.portablefortheelderlybackground.pojo.welcome.welcome;
import com.example.portablefortheelderlybackground.vo.welcomeVo.welcomeVo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = mapTest.class)
public class mapTest {
    @Test
    public void test() {
        List<welcome> list = new ArrayList<>();
        list.add(new welcome());
        list.add(new welcome());
        list.add(new welcome());
        //测试映射
        List<welcomeVo> welcomeVos = list.stream().map(t -> new welcomeVo()).toList();
        System.out.println(welcomeVos);
    }
}
