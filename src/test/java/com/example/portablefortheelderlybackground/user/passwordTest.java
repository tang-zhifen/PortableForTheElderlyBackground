package com.example.portablefortheelderlybackground.user;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Arrays;

@SpringBootTest(classes = passwordTest.class)
public class passwordTest {

    @Test
    public void test() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encode = bCryptPasswordEncoder.encode("20030317TZF");
        System.out.println(encode);
        System.out.println(bCryptPasswordEncoder.matches("$2a$10$HHt6Bqed88iIm4hAaGWuHuOcmuIl55vcsIedOu5u7h.lcX9nh0/Ly", "$2a$10$HHt6Bqed88iIm4hAaGWuHuOcmuIl55vcsIedOu5u7h.lcX9nh0/Ly"));
    }

    @Test
    public void my() {
        int[] a = new int[]{3, 2, 1};
        Arrays.sort(a);
        for (int i : a) {
            System.out.println(i);
        }
    }

    @Test
    public void alternateDigitSum() {
        String nums = 521 + "";
        boolean flag = true;
        int sum = 1;
        sum = -sum;
        System.out.println(sum);
    }
}
