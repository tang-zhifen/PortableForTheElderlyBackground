package com.example.portablefortheelderlybackground.Enum.Message;

public enum ReturnMessageEnum {
    //资源请求成功
    SUCCESS(200, "资源请求成功"),

    NOT_FOUNT(404, "资源未找到"),
    NOT_AUTHENTICATION(403, "请先登录"),
    NOT_DATA(200, "未查询到数据"),
    NOT_AUTHORIZATION(403, "你未拥有对应的权限"),
    SERVER_ERROR(500, "服务器出现错误,请稍后再试"),
    PHONE_CODE(200, "验证码5分钟类有效!"),
    PHONE_CODE_ERROR(0, "验证码错误或账户不存在"),
    DELETE_SUCCESS(200, "删除数据成功!"),
    DELETE_ERROR(0, "删除数据失败"),
    UPDATE_SUCCESS(200, "修改成功"),
    UPDATE_ERROR(0, "修改失败"),
    ID_NOTNULL(0, "id或者ids不能为空"),
    NULL_POINTER(0, "空指针异常,不存在对应的数据"),
    SAVE_ERROR(0, "数据保存失败"),
    SAVE_SUCCESS(200, "数据保存成功"),
    TOKEN_INVALIDATION(403, "token已失效或不存在"),
    TOKEN_REFRESHSUCCESS(200, "token刷新成功"),
    TOKEN_REFRESHFAIL(403, "token刷新失败");

    private ReturnMessageEnum(Integer code, String message) {
        this.message = message;
        this.code = code;
    }

    private String message;
    private Integer code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
