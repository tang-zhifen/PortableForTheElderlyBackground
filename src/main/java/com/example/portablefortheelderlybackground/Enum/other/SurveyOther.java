package com.example.portablefortheelderlybackground.Enum.other;


public enum SurveyOther {
    IS_CURRENTLY(1, "当前调查问卷"), NOT_CURRENTLY(0, "不是当前问卷");

    SurveyOther(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
