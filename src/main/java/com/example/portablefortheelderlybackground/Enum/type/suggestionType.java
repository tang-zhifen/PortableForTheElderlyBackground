package com.example.portablefortheelderlybackground.Enum.type;

public enum suggestionType {
    PESSIMISTIC("消极", "消极待工"),
    POSITIVE("积极", "积极向上");

    private suggestionType(String TYPE, String DESCRIPTION) {
        this.TYPE = TYPE;
        this.DESCRIPTION = DESCRIPTION;
    }

    private String TYPE;
    private String DESCRIPTION;

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }
}
