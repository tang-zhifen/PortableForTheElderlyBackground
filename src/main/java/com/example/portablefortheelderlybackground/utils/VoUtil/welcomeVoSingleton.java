package com.example.portablefortheelderlybackground.utils.VoUtil;

import com.example.portablefortheelderlybackground.vo.welcomeVo.welcomeVo;

public class welcomeVoSingleton {
    private welcomeVoSingleton() {

    }

    private static volatile welcomeVo singleton;

    public static welcomeVo getInstance() {
        if (singleton == null) {
            synchronized (welcomeVoSingleton.class) {
                if (singleton == null) {
                    singleton = new welcomeVo();
                    return singleton;
                }
            }
        }
        return singleton;
    }
}
