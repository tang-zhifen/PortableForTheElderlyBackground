package com.example.portablefortheelderlybackground.utils;

import com.example.portablefortheelderlybackground.pojo.sport.sport;

public class SportSingleton {
    private SportSingleton() {

    }

    private static volatile sport instance;

    //双重检查锁机制保证获取的实例单一
    public static sport getInstance() {
        if (instance == null) {
            synchronized (SportSingleton.class) {
                if (instance == null) {
                    instance = new sport();
                    return instance;
                }
            }
        }
        return instance;
    }
}
