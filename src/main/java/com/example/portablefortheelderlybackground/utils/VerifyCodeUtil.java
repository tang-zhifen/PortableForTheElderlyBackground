package com.example.portablefortheelderlybackground.utils;

import cn.hutool.core.util.RandomUtil;
import org.springframework.util.StringUtils;

/**
 * @author 86187
 * @description: 生成加密密码和验证码
 * @date: 2023/7/4 14:04
 */

public class VerifyCodeUtil {
    //    生成短信验证码
    public static String generatePhoneCode() {
        return RandomUtil.randomNumbers(6);
    }

    public static boolean VerifyCodeEquals(String per_code, String suf_code) {
        if (StringUtils.hasLength(per_code)) {
            return per_code.equals(suf_code);
        }
        return false;
    }
}
