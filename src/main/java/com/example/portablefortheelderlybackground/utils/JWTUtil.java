package com.example.portablefortheelderlybackground.utils;

import io.jsonwebtoken.*;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

//需要springSecurity依赖以及lombok以及jwt依赖
//注册组件
//@Data和@ConfigurationProperties结合使用用于在yaml中对其常量进行注入
@Data
@ConfigurationProperties(prefix = "jwt.data")
@Slf4j
public class JWTUtil {
    //创建对象主题
    private static final String CLAIM_KEY_SUBJECT = "subject";
    //创建创建时间
    private static final String CLAIM_KEY_CREATED = "created";
    //创建用户名
    private static final String CLAIM_KEY_USERNAME = "username";
    public static final String CLAIM_KEY_USERDETAILS = "userDetails";

    //@Value这个注解一定要引入spring-boot-starter-validation才能使用
    //@Value注解可以代替@Data和@ConfigurationProperties结合
    //这两个二者选一即可
    //我建议使用@Data和@ConfigurationProperties结合
    //@Value("${jwt.data.SECRET}")
    private String SECRET;//创建加密盐

    //过期时间
    private Long expiration;

    //刷新token的持续时间
    private Integer refreshTime;

    //根据用户名生成token
    //传入的是使用SpringSecurity里的UserDetails
    public String createToken(UserDetails userDetails, boolean isRefreshToken) {
        HashMap<String, Object> claims = new HashMap<>();
        //存放一个userDetails主体和用户名
        claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
        claims.put(CLAIM_KEY_USERDETAILS, userDetails);
        claims.put(CLAIM_KEY_CREATED, new Date());
        return createToken(claims, isRefreshToken);//装入map
    }

    //根据token获取用户名
    public String getUsernameFromToken(String token) {
        String username = "";
        try {
            Claims claims = getClaimsFromToken(token);
            username = claims.get(CLAIM_KEY_USERNAME, String.class);
        } catch (Exception e) {
            username = null;
            log.info("error:{}", "用户名未能获取 from token");
        }
        return username;
    }

    //从token中获取荷载
    public Claims getClaimsFromToken(String token) {
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException e) {
            e.printStackTrace();
        } catch (UnsupportedJwtException e) {
            e.printStackTrace();
        } catch (MalformedJwtException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return claims;
    }


    //根据负载生成jwt token
    private String createToken(Map<String, Object> claims, boolean isRefreshToken) {
        //jjwt构建jwt builder
        //设置信息,过期时间,主题,signNature
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(CLAIM_KEY_SUBJECT)
                .setExpiration(expirationDate(isRefreshToken))
                .signWith(SignatureAlgorithm.HS256, SECRET)
                .compact();
    }

    //生成token失效时间
    private Date expirationDate(boolean isRefreshToken) {
        //失效时间为：系统当前毫秒数+我们设置的时间（s）*1000=》毫秒
        long time = isRefreshToken ? expiration * 1000 * refreshTime : expiration * 1000;
        //其实就是未来7天
        return new Date(System.currentTimeMillis() + time);
    }

    //判断token是否有效
    public boolean validateToken(String token, UserDetails userDetails) {
        //判断token是否过期
        //判断token是否和userDetails中的一致
        //我们要做的 是先获取用户名
        String username = getUsernameFromToken(token);
        return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
    }

    //判断token、是否失效
    //失效返回true
    public boolean isTokenExpired(String token) {
        Date expiredDate = getExpiredDateFeomToken(token);
        return expiredDate.before(new Date());
    }

    //从荷载中获取时间
    public Date getExpiredDateFeomToken(String token) {
        Claims claims = getClaimsFromToken(token);
        return claims.getExpiration();
    }

    //判断token是否可以被刷新
    //过期（销毁）就可以
    public boolean canBeRefreshed(String token) {
        return !isTokenExpired(token);
    }

    //刷新token
    public String refreshToken(String token) {
        if (canBeRefreshed(token)) {
            Claims claims = getClaimsFromToken(token);
            //修改为当前时间
            claims.put(CLAIM_KEY_CREATED, new Date());
            return createToken(claims, false);
        }
        return "can not refreshToken";
    }

}