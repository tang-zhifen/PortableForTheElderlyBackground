package com.example.portablefortheelderlybackground.utils.PageUtil;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PageGenerateUtil {
    public static <T> Page<T> PageVoGenerate(T page) {
        log.info("vo对象:{}", page.getClass().getName());
        return new Page<T>();
    }
}
