package com.example.portablefortheelderlybackground.utils;

import com.example.portablefortheelderlybackground.pojo.dish.dish;

/**
 * @author 86187
 * @description: 懒汉式加载,双重检查锁
 * @date: 2023/6/15 21:10
 */

public class DishSingleton {
    private DishSingleton() {

    }

    private static volatile dish instance;

    //双重检查锁机制保证获取的实例单一
    public static dish getInstance() {
        if (instance == null) {
            synchronized (SportSingleton.class) {
                if (instance == null) {
                    instance = new dish();
                    return instance;
                }
            }
        }
        return instance;
    }
}
