package com.example.portablefortheelderlybackground.utils;

import com.example.portablefortheelderlybackground.common.Message.ResultMessage;
import org.springframework.util.ObjectUtils;

import static com.example.portablefortheelderlybackground.Enum.Message.ReturnMessageEnum.*;

/**
 * @author 86187
 * @description: 返回结果封装类, status默认处理成功时的信息和状态码
 * @date: 2023/7/12 16:54
 */

public class Response<T> {
    //    响应状态
    public Integer code;
    //    存放的数据
    public T data;
    //    返回的数据描述
    public String message;

    /**
     * @param data 数据
     * @return com.example.portablefortheelderlybackground.utils.R<T>
     * @description: 响应成功的数据处理
     * @author 86187
     * @date: 2023/4/1 17:15
     */

    public static <T> Response<T> success(T data) {
        return Response.success(data, SUCCESS.getMessage());
    }

    /**
     * @param data    数据
     * @param message 消息
     * @return com.example.portablefortheelderlybackground.utils.R<T>
     * @description: success重载方法
     * @author 86187
     * @date: 2023/5/19 12:33
     */

    public static <T> Response<T> success(T data, String message) {
        return Response.success(data, SUCCESS.getCode(), message);
    }

    public static <T> Response<T> success(T data, Integer code, String message) {
        Response<T> res = new Response<>();
        res.code = code == null ? SUCCESS.getCode() : code;
        res.message = message == null ? SUCCESS.getMessage() : message;
        res.data = data;
        return res;
    }

    /**
     * @param message 消息
     * @return com.example.portablefortheelderlybackground.utils.R<T>
     * @description: 错误响应的返回
     * @author 86187
     * @date: 2023/4/1 17:17
     */

    public static <T> Response<T> error(String message) {
        return Response.error(null, message);
    }

    /**
     * @param data    数据
     * @param message 消息
     * @return com.example.portablefortheelderlybackground.utils.R<T>
     * @description: 定义error重载方法
     * @author 86187
     * @date: 2023/5/19 23:29
     */

    public static <T> Response<T> error(T data, String message) {
        return Response.error(data, NOT_FOUNT.getCode(), message);
    }

    /**
     * @param code    错误码
     * @param data    数据
     * @param message 信息
     * @return com.example.portablefortheelderlybackground.utils.R<T>
     * @author 86187
     * @date: 2023/7/12 16:29
     */

    public static <T> Response<T> error(T data, Integer code, String message) {
        Response<T> res = new Response<>();
        res.code = code == null ? NOT_FOUNT.getCode() : code;
        res.message = message == null ? NOT_FOUNT.getMessage() : message;
        res.data = data;
        return res;
    }

    public static <T> Response<T> status(T data, String message) {
        return Response.status(data, SUCCESS.getCode(), message);
    }

    public static <T> Response<T> status(T data, Integer code, String message) {
        if (ObjectUtils.isEmpty(data)) {
            return Response.error(data, code == null ? NOT_DATA.getCode() : code, message == null ? NOT_DATA.getMessage() : message);
        }
        return Response.success(data, code == null ? SUCCESS.getCode() : code, message == null ? SUCCESS.getMessage() : message);
    }

    /**
     * @param data 数据
     * @return com.example.portablefortheelderlybackground.utils.R<T>
     * @description: 根据data的状态返回对应的状态
     * @author 86187
     * @date: 2023/7/6 20:04
     */

    public static <T> Response<T> status(T data) {
        return Response.status(data, null);
    }

    public static Response<Boolean> status(boolean status, String message) {
        return status ? Response.success(true, message) : Response.error(false, message);
    }

    public static Response<Boolean> status(boolean status, Integer code, String message) {
        return status ? Response.success(true, code, message) : Response.error(false, code, ResultMessage.error_message);
    }

    /**
     * @param status 状态
     * @return com.example.portablefortheelderlybackground.utils.R<java.lang.Boolean>
     * @description: 根据状态决定返回对应的结果
     * @author 86187
     * @date: 2023/7/6 20:04
     */

    public static Response<Boolean> status(boolean status) {
        return Response.status(status, null);
    }
}
