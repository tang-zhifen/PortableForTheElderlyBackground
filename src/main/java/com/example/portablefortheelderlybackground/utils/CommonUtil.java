package com.example.portablefortheelderlybackground.utils;

public class CommonUtil {
    public static <T> T ifAbsent(T t, T defaultValue) {
        return t == null ? defaultValue : t;
    }
}
