package com.example.portablefortheelderlybackground.pojo.history;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("look_history")
public class lookHistory implements Serializable {
    @Serial
    private static final long serialVersionUID = 6515613126420035060L;
    @TableId
    private String id;
    private String userId;
    private String paperId;
    private LocalDateTime lookTime;
    private Integer status;
}
