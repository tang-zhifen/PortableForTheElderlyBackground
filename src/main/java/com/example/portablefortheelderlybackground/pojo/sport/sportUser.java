package com.example.portablefortheelderlybackground.pojo.sport;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("sport_user")
public class sportUser implements Serializable {
    private static final long serialVersionUID = -2173911833076255862L;
    private String id;
    private String sportId;
    private String userId;
    private String createTime;
    private String updateTime;
}
