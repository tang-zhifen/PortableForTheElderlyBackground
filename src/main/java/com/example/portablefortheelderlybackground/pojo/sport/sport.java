package com.example.portablefortheelderlybackground.pojo.sport;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@TableName("sport")
@Data
public class sport implements Serializable {
    @Serial
    private static final long serialVersionUID = 6016737766749517408L;
    @Pattern(regexp = "[0-9]{18,20}")
    private String id;
    @NotBlank(message = "名称不能为空", groups = {AddGroup.class})
    private String name;
    @NotBlank(message = "类型不能为空", groups = {AddGroup.class})
    private String type;
    @NotBlank(message = "描述不能为空", groups = {AddGroup.class})
    private String description;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    private String imageUrl;
}
