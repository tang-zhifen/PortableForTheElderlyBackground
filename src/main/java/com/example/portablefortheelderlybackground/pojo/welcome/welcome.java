package com.example.portablefortheelderlybackground.pojo.welcome;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@TableName("welcome")
@Data
@JsonIgnoreProperties(value = {"createTime", "updateTime"})
public class welcome implements Serializable {
    @Serial
    private static final long serialVersionUID = -3973161108945635468L;
    @TableId
    private String id;
    private String imageUrl;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    private Integer status;
    private Integer remove;
    private String type;

    public welcome setIsDelete(Integer isDelete) {
        this.remove = isDelete;
        return this;
    }
}
