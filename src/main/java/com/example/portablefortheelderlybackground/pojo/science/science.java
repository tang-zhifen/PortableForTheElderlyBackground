package com.example.portablefortheelderlybackground.pojo.science;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.config.group.UpdateGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

//科普表
@Data
@TableName("popular_science")
public class science implements Serializable {
    @Serial
    private static final long serialVersionUID = 114163387269336248L;
    @TableId
    @NotBlank(message = "id不能爲空", groups = {UpdateGroup.class})
    private String id;
    @NotBlank(message = "消息不能為空", groups = {AddGroup.class})
    private String message;
    @NotBlank(message = "类型不能為空", groups = {AddGroup.class})
    private String type;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    private String createBy;
    @NotBlank(message = "来源不能為空", groups = {AddGroup.class})
    private String source;
    @NotBlank(message = "标题不能為空", groups = {AddGroup.class})
    private String title;
}
