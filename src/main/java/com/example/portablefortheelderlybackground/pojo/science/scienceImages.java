package com.example.portablefortheelderlybackground.pojo.science;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
@TableName("scienceimages")
public class scienceImages implements Serializable {
    @Serial
    private static final long serialVersionUID = 6679061203053160198L;
    private String id;
    private String images;
    private String scienceId;
    private String content;
    private String videos;

}
