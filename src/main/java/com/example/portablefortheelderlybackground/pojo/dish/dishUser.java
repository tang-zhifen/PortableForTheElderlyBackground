package com.example.portablefortheelderlybackground.pojo.dish;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@TableName("dish_user")
@Data
public class dishUser implements Serializable {
    @Serial
    private static final long serialVersionUID = -7931725872137523110L;
    private String id;
    private String dishId;
    private String userId;
    private String createTime;
    private String updateTime;
}
