package com.example.portablefortheelderlybackground.pojo.dish;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.config.group.UpdateGroup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("dish")
@JsonIgnoreProperties(value = {"crateTime", "updateTime"})
public class dish implements Serializable {
    @Serial
    private static final long serialVersionUID = 5458162526638317631L;
    @NotBlank(message = "id不能为空", groups = {UpdateGroup.class})
    private String id;
    @NotBlank(message = "name不能为空", groups = {AddGroup.class})
    private String name;
    @NotBlank(message = "type不能为空", groups = {AddGroup.class})
    private String type;
    @NotBlank(message = "介绍不能为空", groups = {AddGroup.class})
    private String introduce;
    private String helper;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    private String imageUrl;
}
