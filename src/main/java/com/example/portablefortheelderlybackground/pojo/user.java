package com.example.portablefortheelderlybackground.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.config.group.UpdateGroup;
import com.example.portablefortheelderlybackground.config.group.queryGroup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.apache.ibatis.annotations.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serial;
import java.io.Serializable;

//用户表
@Data
@TableName("user")
public class user implements Serializable {
    @Serial
    private static final long serialVersionUID = 815054258730451992L;
    @TableId
    @NotBlank(message = "id不能为空", groups = {UpdateGroup.class})
    private String id;
    @NotEmpty(message = "姓名不能为空", groups = {AddGroup.class})
    private String name;
    private Integer age;
    //性别为0或者1
    private String sex;
    private String phone;
    @NotBlank(message = "密码不能为空", groups = {AddGroup.class})
    private String password;
    //头像字段
    private String avatarUrl;
}
