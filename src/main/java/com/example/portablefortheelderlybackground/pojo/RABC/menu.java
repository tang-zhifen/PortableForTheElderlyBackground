package com.example.portablefortheelderlybackground.pojo.RABC;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@TableName("menu")
@Data
public class menu implements Serializable {
    @Serial
    private static final long serialVersionUID = -2240491614716605431L;

    @TableId
    private String id;
    private String menuName;
    private String authentication;
}
