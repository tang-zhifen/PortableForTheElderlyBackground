package com.example.portablefortheelderlybackground.pojo.RABC;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("role")
public class role implements Serializable {
    @Serial
    private static final long serialVersionUID = 5172418391033994111L;
    @TableId
    private String id;

    private String name;

    private String description;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;
}
