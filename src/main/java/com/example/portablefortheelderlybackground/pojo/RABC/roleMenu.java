package com.example.portablefortheelderlybackground.pojo.RABC;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
@TableName("role_menu")
public class roleMenu implements Serializable {
    @Serial
    private static final long serialVersionUID = 279382804281502588L;
    private String roleId;
    private String menuId;
}
