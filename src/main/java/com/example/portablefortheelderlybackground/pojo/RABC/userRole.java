package com.example.portablefortheelderlybackground.pojo.RABC;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

@TableName("user_role")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class userRole implements Serializable {
    @Serial
    private static final long serialVersionUID = -5137414192092264408L;
    @TableId
    private String userId;

    private String roleId;

}
