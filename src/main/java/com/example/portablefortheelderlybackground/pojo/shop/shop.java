package com.example.portablefortheelderlybackground.pojo.shop;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("shop")
@Data
//@Accessors(chain = true)
public class shop {
    @TableId
    private String id;

    private Integer count;
    private String description;
}
