package com.example.portablefortheelderlybackground.pojo.weather;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("wea_city")
public class weatherCity implements Serializable {
    @Serial
    private static final long serialVersionUID = -6963114708066656072L;
    @TableId
    private String id;
    private String cityid;
    private String city;
    @TableField(value = "update_time")
    private String update_time;
    @TableField(value = "create_time")
    private LocalDateTime create_time;
}
