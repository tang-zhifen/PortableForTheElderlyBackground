package com.example.portablefortheelderlybackground.pojo.weather;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@TableName("wea_data")
@Data
public class weatherData implements Serializable {

    @Serial
    private static final long serialVersionUID = -26903453670309174L;
    @TableId
    private String id;
    private String date;
    private String wea;
    @TableField(value = "tem_day")
    private String tem_day;
    @TableField(value = "tem_night")
    private String tem_night;
    private String win;
    @TableField(value = "win_speed")
    private String win_speed;
}
