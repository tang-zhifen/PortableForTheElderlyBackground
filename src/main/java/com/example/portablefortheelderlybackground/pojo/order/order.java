package com.example.portablefortheelderlybackground.pojo.order;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("order_shop")
public class order {
    @TableId
    private String id;
    private String userId;
    private String shopId;
}
