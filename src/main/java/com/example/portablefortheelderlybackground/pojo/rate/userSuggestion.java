package com.example.portablefortheelderlybackground.pojo.rate;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.LocalDateTime;

@TableName(value = "user_suggestion")
@Data
public class userSuggestion {
    @TableId
    private String id;
    @NotBlank(message = "内容不能为空", groups = {AddGroup.class})
    private String content;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    private String type;
    private String userId;
}
