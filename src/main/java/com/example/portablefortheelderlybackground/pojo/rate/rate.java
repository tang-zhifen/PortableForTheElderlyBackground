package com.example.portablefortheelderlybackground.pojo.rate;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("rate")
public class rate implements Serializable {

    @Serial
    private static final long serialVersionUID = -7668280484157772612L;
    @TableId
    private String id;
    @NotBlank(message = "value不能为空", groups = {AddGroup.class})
    private String value;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @NotBlank(message = "类型不能为空", groups = {AddGroup.class})
    private String type;
    @TableField(fill = FieldFill.INSERT,value = "is_delete")
    private String delete;
}
