package com.example.portablefortheelderlybackground.pojo.question;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.config.group.UpdateGroup;
import com.example.portablefortheelderlybackground.config.group.queryGroup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("question")
@Accessors(chain = true)//开启建造者模式
@JsonIgnoreProperties(value = {"createTime", "updateTime"})
public class question implements Serializable {
    @Serial
    private static final long serialVersionUID = 4713353997560293250L;
    @TableId
    private String id;
    @NotBlank(message = "问题名称不能为空", groups = {AddGroup.class, queryGroup.class, UpdateGroup.class})
    private String questionName;
    @NotBlank(message = "问题来源不能为空", groups = {AddGroup.class})
    private String source;
    @NotBlank(message = "问题类型不能为空", groups = {AddGroup.class})
    private String type;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT, value = "is_delete")
    private Integer delete;
    @NotBlank(message = "是否多选不能为空", groups = {AddGroup.class})
    @TableField(value = "is_multiple")
    private Integer multiple;
    @NotBlank(message = "正确答案id不能为空", groups = {UpdateGroup.class})
    @TableField(value = "answer_id")
    private String answerId;

}
