package com.example.portablefortheelderlybackground.pojo.question;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("answer")
//忽略以下字段的json序列化
@JsonIgnoreProperties(value = {"createTime", "updateTime"})
public class answer implements Serializable {
    @Serial
    private static final long serialVersionUID = -552941841315007487L;
    @TableId
    private String id;
    @NotBlank(message = "答案不能为空", groups = {AddGroup.class})
    private String answerContent;
    //添加字段填充
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT,value = "is_delete")
    private Integer delete;
    private String questionId;
}
