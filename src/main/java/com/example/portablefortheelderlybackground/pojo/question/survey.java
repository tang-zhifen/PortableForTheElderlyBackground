package com.example.portablefortheelderlybackground.pojo.question;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName("survey")
@JsonInclude(JsonInclude.Include.NON_NULL)//如果字段不为null则字段会进行序列化
public class survey implements Serializable {
    @Serial
    private static final long serialVersionUID = 5930993137365511250L;
    @TableId
    private String id;
    private String name;
    private Integer count;
    private Integer peoples;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime createTime;
    private String despriction;
    @TableField(fill = FieldFill.INSERT)
    private Integer isDelete;
    @NotBlank(message = "类型不能为空")
    private String type;
    @NotEmpty(message = "current不能为空", groups = AddGroup.class)
    private Integer currently;
}
