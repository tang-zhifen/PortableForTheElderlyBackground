package com.example.portablefortheelderlybackground.pojo.question;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class surveyRelationQuestion {
    @TableId
    private String id;
    private String questionId;
    private Integer status;
    private LocalDateTime createTime;
    private LocalDateTime createBy;
}
