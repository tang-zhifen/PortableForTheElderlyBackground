package com.example.portablefortheelderlybackground.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ObjectUtils;

import java.io.Serial;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)//开启建造者模式
public class userDetailImpl implements UserDetails {

    private user user;
    //中间list转换流
    List<String> permission;
    //权限列表
    @JsonIgnore
    List<GrantedAuthority> authorities;//忽略对其的序列化和反序列化
    @Serial
    private static final long serialVersionUID = 4938659927679414682L;

    public userDetailImpl(user user, List<String> permission) {
        this.user = user;
        this.permission = permission;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (!ObjectUtils.isEmpty(authorities)) {
            return authorities;
        }
        authorities = permission.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        return authorities;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getName();
    }

    @Override
    public boolean isAccountNonExpired() {//用户是否过期
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {//用户是否被锁定
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {//用户是否过期
        return true;
    }

    @Override
    public boolean isEnabled() {//用户是否启用
        return true;
    }
}
