package com.example.portablefortheelderlybackground.Filter;

import com.example.portablefortheelderlybackground.common.redis.redisKey;
import com.example.portablefortheelderlybackground.config.Exception.user.TokenInvalidationException;
import com.example.portablefortheelderlybackground.config.Security.RequestValidationHeader;
import com.example.portablefortheelderlybackground.config.userDetails.AuthThread;
import com.example.portablefortheelderlybackground.pojo.userDetailImpl;
import com.example.portablefortheelderlybackground.utils.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author 86187
 * @description: token效验过滤器, 将会注册到spring和security中, 在放行路径, 会经过spring注册的该过滤器
 * @date: 2023/9/13 12:01
 */

@Component
@Slf4j
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
    private final JWTUtil jwtUtil;

    private final RedisTemplate<String, Object> redisTemplate;
    private final RequestValidationHeader validationHeader;

    //构造器注入
    @Autowired
    public JwtAuthenticationTokenFilter(JWTUtil jwtUtil, RedisTemplate<String, Object> redisTemplate, RequestValidationHeader validationHeader) {
        this.jwtUtil = jwtUtil;
        this.redisTemplate = redisTemplate;
        this.validationHeader = validationHeader;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String token = request.getHeader(validationHeader.Authentication);
        String refreshToken = request.getHeader(validationHeader.refreshAuthentication);
        //如果未携带token请求头,或者携带refreshToken请求头,直接放行
        if (!StringUtils.hasText(token) && !StringUtils.hasText(refreshToken)) {
            filterChain.doFilter(request, response);//放行进行鉴权
            return;
        }
        //如果refreshToken不为空,则优先使用refreshToken进行解析
        String parseToken = StringUtils.hasText(refreshToken) ? refreshToken : token;
        String usernameFromToken = jwtUtil.getUsernameFromToken(parseToken);
        //判断token是否失效
        if (!StringUtils.hasText(usernameFromToken) && !jwtUtil.isTokenExpired(parseToken)) {
            throw new TokenInvalidationException(validationHeader.Authentication + "已失效,或者不存在");
        }
        //从redis获取缓存
        userDetailImpl userDetail = (userDetailImpl) redisTemplate.opsForValue().get(redisKey.usernameId + usernameFromToken);
        //对userDetail进行判断
        if (ObjectUtils.isEmpty(userDetail)) {
            throw new NullPointerException("用户名不存在,或者缓存失效");
        }
        //为请求上下文添加权限
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetail, null, userDetail.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        //为单次请求线程添加用户名
        AuthThread.set(userDetail.getUsername());
        filterChain.doFilter(request, response);
    }
}
