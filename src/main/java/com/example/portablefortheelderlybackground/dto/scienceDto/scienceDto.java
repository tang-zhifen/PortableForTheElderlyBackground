package com.example.portablefortheelderlybackground.dto.scienceDto;

import com.example.portablefortheelderlybackground.pojo.science.science;
import com.example.portablefortheelderlybackground.pojo.science.scienceImages;
import lombok.Data;

import java.io.Serial;
import java.util.List;

@Data
public class scienceDto extends science {
    @Serial
    private static final long serialVersionUID = 1001499805544684268L;
    private List<scienceImages> scienceImagesList;
}
