package com.example.portablefortheelderlybackground.dto.historyDto;

import com.example.portablefortheelderlybackground.pojo.history.lookHistory;

import java.io.Serial;

/**
 * @author 86187
 * @description: 与数据库交互的dto, 保存数据, 不需要其他的属性
 * @date: 2023/10/16 22:54
 */

public class lookDto extends lookHistory {
    @Serial
    private static final long serialVersionUID = -4845807053928756632L;

}
