package com.example.portablefortheelderlybackground.dto.weatherDto;

import com.example.portablefortheelderlybackground.pojo.weather.weatherCity;
import com.example.portablefortheelderlybackground.pojo.weather.weatherData;
import lombok.Data;
import java.io.Serial;
import java.util.List;

@Data
public class weatherDto extends weatherCity {
    @Serial
    private static final long serialVersionUID = 7507342063615978291L;
    List<weatherData> weatherDataList;
}
