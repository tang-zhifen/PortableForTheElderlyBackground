package com.example.portablefortheelderlybackground;

import com.example.portablefortheelderlybackground.common.environment.EnvironmentHttp;
import com.example.portablefortheelderlybackground.config.weatherAPI.tianqiParams;
import com.example.portablefortheelderlybackground.utils.JWTUtil;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.util.StringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;

@SpringBootApplication
@MapperScan(basePackages = "com.example.portablefortheelderlybackground.mapper")
@Slf4j
@EnableConfigurationProperties(value = {JWTUtil.class, tianqiParams.class})
public class PortableForTheElderlyBackgroundApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(PortableForTheElderlyBackgroundApplication.class, args);
        System.out.println("========  接口文档地址 ========");
        ConfigurableEnvironment environment = run.getEnvironment();
        //获取项目端口号
        String port = environment.getProperty("server.port");
        //获取项目根路径
        String path = environment.getProperty("server.servlet.context-path");
        if (!StringUtils.hasLength(path)) {
            path = "";
        }
        //获取项目主机名称
        try {
            // 获取项目访问主机名称
            String hostAddress = InetAddress.getLocalHost().getHostAddress();
            //内网访问地址
            System.out.println("内网访问地址:" + EnvironmentHttp.HTTP + hostAddress + ":" + port + path + "/doc.html");
            //本地访问地址
            System.out.println("本地访问地址:" + EnvironmentHttp.HTTP + "localhost:" + port + path + "/doc.html");
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

}
