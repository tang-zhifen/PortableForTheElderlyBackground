package com.example.portablefortheelderlybackground.common.User;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.portablefortheelderlybackground.common.StringCommon;
import com.example.portablefortheelderlybackground.common.redis.redisKey;
import com.example.portablefortheelderlybackground.config.Security.RequestValidationHeader;
import com.example.portablefortheelderlybackground.pojo.RABC.menu;
import com.example.portablefortheelderlybackground.pojo.RABC.roleMenu;
import com.example.portablefortheelderlybackground.pojo.RABC.userRole;
import com.example.portablefortheelderlybackground.pojo.user;
import com.example.portablefortheelderlybackground.pojo.userDetailImpl;
import com.example.portablefortheelderlybackground.sevice.menuService;
import com.example.portablefortheelderlybackground.sevice.roleMenuService;
import com.example.portablefortheelderlybackground.sevice.userRoleService;
import com.example.portablefortheelderlybackground.utils.JWTUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author 86187
 * @description: 封装userDetails
 * @date: 2023/7/15 21:41
 */

public class loadUserDetails {
    public static UserDetails getUserDetails(userRoleService userRoleService, roleMenuService roleMenuService, menuService menuService, user user) {
        userRole userRoleById = userRoleService.getById(user.getId());
        //根据roleId查询role_menu表
        LambdaQueryWrapper<roleMenu> roleMenuLambdaQueryWrapper = new LambdaQueryWrapper<>();
        roleMenuLambdaQueryWrapper.eq(StringUtils.hasText(userRoleById.getRoleId()), roleMenu::getRoleId, userRoleById.getRoleId());
        List<roleMenu> roleMenuList = roleMenuService.list(roleMenuLambdaQueryWrapper);
        //查询出roleMenuList
        List<String> menuIdList = new ArrayList<>();
        for (roleMenu roleMenu : roleMenuList) {
            menuIdList.add(roleMenu.getMenuId());
        }
        //根据menuIdList查询menu获得权限
        //构建queryWrapper
        LambdaQueryWrapper<menu> menuQueryWrapper = new LambdaQueryWrapper<>();
        menuQueryWrapper.in(!ObjectUtils.isEmpty(menuIdList), menu::getId, menuIdList);
        List<menu> menuList = menuService.list(menuQueryWrapper);
        //将查询出的menuList进行处理
        List<String> permission = new ArrayList<>();
        for (menu menu : menuList) {//遍历将权限存放进list
            permission.add(menu.getAuthentication());
        }
        return new userDetailImpl(user, permission);
    }

    public static Map<String, Object> returnUserInfoAndCache(JWTUtil jwtUtil, userDetailImpl userDetails, RedisTemplate<String, Object> redisTemplate, RequestValidationHeader validationHeader) {
        //需要生成 token
        String token = jwtUtil.createToken(userDetails, false);
        //生成refresh_token
        String refreshToken = jwtUtil.createToken(userDetails, true);
        //创建map
        Map<String, Object> map = new HashMap<>();
        map.put(validationHeader.Authentication, token);
        map.put(validationHeader.refreshAuthentication, refreshToken);
        map.put("user", userDetails.getUser());
        //用户信息注入redis
        redisTemplate.opsForValue().set(redisKey.usernameId + userDetails.getUsername(), userDetails, StringCommon.TimeOut, TimeUnit.HOURS);
        return map;
    }
}
