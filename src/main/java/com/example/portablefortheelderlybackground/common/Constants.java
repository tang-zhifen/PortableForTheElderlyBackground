package com.example.portablefortheelderlybackground.common;

public class Constants {
    public static final Integer NOT_DELETE = 0;
    public static final Integer IS_DELETE = 1;
    public static final Integer INSERT_SUCCESS = 0;
    public static final Integer INSERT_FAIL = -1;
    public static final Integer DEFAULT_PAGE = 1;
    public static final Integer DEFAULT_PAGESIZE = 20;

}
