package com.example.portablefortheelderlybackground.common.Message;

import org.springframework.util.StringUtils;

/**
 * @author 86187
 * @description: 对返回消息进行统一管理
 * @date: 2023/7/6 9:56
 */

public class ResultMessage {
    public static final String success_message = "请求响应成功";
    public static final String error_message = "请求响应失败";
    public static final String default_message = "请求处理完毕!";

    public static String setSelfMessage(String selfMessage) {
        if (!StringUtils.hasLength(selfMessage)) {
            return default_message;
        }
        return selfMessage;
    }

    public static String getDefaultMessage(String message) {
        if (!StringUtils.hasLength(message)) {
            message = ResultMessage.default_message;
        }
        return message;
    }
}
