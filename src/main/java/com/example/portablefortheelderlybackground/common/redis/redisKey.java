package com.example.portablefortheelderlybackground.common.redis;

import org.springframework.util.StringUtils;

public class redisKey {

    public static final String scienceQueryKey = "science:list:";
    public static final String scienceKey = "science:singleton:";
    public static final String usernameId = "username:";

    public static final String dishQueryKey = "dish:list:";
    public static final String dishKey = "dish:singleton:";
    public static final String sportQueryKey = "sport:list:";
    public static final String sportKey = "sport:singleton:";
    public static final String answerKey = "answer:singleton:";
    public static final String answerQueryKey = "answer:list:";
    public static final String questionKey = "question:singleton:";
    public static final String questionQueryKey = "question:list:";
    public static final String surveyKey = "survey:singleton:";
    public static final String rateKey = "rate:list:";
    public static final String sendMessageTimeLimitKey = "sms:";
    public static final String welcomeSwiperKey = "welcome:swiper:";
    public static final String scienceQueryNotFindKey = "science:not_find:";
    public static final String currentSurveyVoById = "survey:current:";

    //    根据type和title生成keyword
    public static String getScienceQueryKeyByTypeAndTitle(String type, String title) {
        String redisKeyWordKey = "";
        if (StringUtils.hasText(type) && StringUtils.hasText(title)) {
            redisKeyWordKey = type + ":" + title + ":";
        } else if (StringUtils.hasText(type)) {
            redisKeyWordKey = type + ":";
        } else if (StringUtils.hasText(title)) {
            redisKeyWordKey = title + ":";
        }
        return redisKeyWordKey;
    }

    //设置过期时间,未命中的情况下,设置缓存时间为2小时
    public static long expireTimeForEmpty = 2;
}
