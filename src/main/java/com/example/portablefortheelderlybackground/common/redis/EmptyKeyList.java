package com.example.portablefortheelderlybackground.common.redis;

import java.util.HashMap;

public class EmptyKeyList {
    private static final HashMap<String, Object> map = new HashMap<>();

    public static Object get(String key) {
        if (isExist(key)) {
            return map.get(key);
        }
        return null;
    }

    public static void set(String key, Object data) {
        map.put(key, data);
    }

    public static void set(String key) {
        set(key, null);
    }

    public static boolean isExist(String key) {
        return map.containsKey(key);
    }
}
