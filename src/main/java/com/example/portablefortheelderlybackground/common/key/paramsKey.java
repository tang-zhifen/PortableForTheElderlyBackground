package com.example.portablefortheelderlybackground.common.key;


/**
 * @author 86187
 * @description: 公共的参数的片段
 * @date: 2023/7/6 17:01
 */

public class paramsKey {
    public static final Integer NUM_ONE = 1;
    public static final Integer NUM_ZERO = 0;
    //是否删除
    public static final Integer NOT_DELETE = 0;
    public static final Integer IS_DELETE = 1;
    //是否展示
    public static final Integer SHOW_STATUS = 1;
    public static final Integer DONT_STATUS = 0;

    //默认页数大小
    public static final Integer PAGE_SIZE = 10;
}
