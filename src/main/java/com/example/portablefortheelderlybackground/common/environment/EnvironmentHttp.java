package com.example.portablefortheelderlybackground.common.environment;


public class EnvironmentHttp {

    //http服务
    public static final String HTTP = "http://";
    //https服务
    public static final String HTTPS = "https://";
}
