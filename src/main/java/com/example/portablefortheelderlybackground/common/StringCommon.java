package com.example.portablefortheelderlybackground.common;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class StringCommon {

    public static String NormalUserRoleId;
    public static String AdminUserRoleId;

    public static long TimeOut;
    //手机验证码的过期时间
    public static long SMSCodeTimeOut;
    //发送手机号的国家前缀
    public static String PREFIX_CHINA_PHONE;
    public static String ThreadUserName;

    public StringCommon() {

    }

    @Value("${sms.module.prefixPhone}")
    public void setPrefixChinaPhone(String prefixChinaPhone) {
        PREFIX_CHINA_PHONE = prefixChinaPhone;
    }

    @Value("${sms.module.minute}")
    public void setSMSCodeTimeOut(long smsCodeTimeOut) {
        SMSCodeTimeOut = smsCodeTimeOut;
    }

    @Value("${cache.redis.aliveTime}")
    public void setTimeOut(long timeOut) {
        TimeOut = timeOut;
    }

    @Value("${userRole.NormalUserRoleId}")
    public void setNormalUserRoleId(String normalUserRoleId) {
        NormalUserRoleId = normalUserRoleId;
    }

    @Value("${userRole.AdminUserRoleId}")
    public void setAdminUserRoleId(String adminUserRoleId) {
        AdminUserRoleId = adminUserRoleId;
    }
}
