package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.weather.weatherData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface weatherDataMapper extends BaseMapper<weatherData> {
}
