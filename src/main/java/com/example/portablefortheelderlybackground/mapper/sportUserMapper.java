package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.sport.sportUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface sportUserMapper extends BaseMapper<sportUser> {
}
