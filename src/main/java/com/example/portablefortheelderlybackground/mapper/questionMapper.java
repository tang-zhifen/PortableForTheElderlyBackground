package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.portablefortheelderlybackground.pojo.question.question;
import com.example.portablefortheelderlybackground.vo.questionVo.topicVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface questionMapper extends BaseMapper<question> {
    IPage<topicVo> getAllTopicVo(IPage<topicVo> page, @Param("type") String type);

    int deleteByIds(@Param("ids") String[] ids);

    IPage<topicVo> getAllTopicVoById(IPage<topicVo> page, @Param("ids") List<String> ids);
}
