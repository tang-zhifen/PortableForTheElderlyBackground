package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.portablefortheelderlybackground.pojo.history.lookHistory;
import com.example.portablefortheelderlybackground.vo.historyVo.lookAtScienceVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface lookHistoryMapper extends BaseMapper<lookHistory> {
    IPage<lookAtScienceVo> getScienceLookHistory(IPage<lookAtScienceVo> page, @Param("id") String id);
}
