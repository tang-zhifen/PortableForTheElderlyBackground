package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.dish.dish;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface dishMapper extends BaseMapper<dish> {
}
