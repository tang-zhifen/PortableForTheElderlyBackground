package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.question.surveyRelationQuestion;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SurveyRelationQuestionMapper extends BaseMapper<surveyRelationQuestion> {
    List<String> getQuestionIdListBySurveyId(@Param("id") String id);

}
