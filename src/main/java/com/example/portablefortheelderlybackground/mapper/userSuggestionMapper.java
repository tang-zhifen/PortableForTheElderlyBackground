package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.rate.userSuggestion;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface userSuggestionMapper extends BaseMapper<userSuggestion> {
}
