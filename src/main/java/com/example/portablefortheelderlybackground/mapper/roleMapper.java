package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.RABC.role;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface roleMapper extends BaseMapper<role> {
}
