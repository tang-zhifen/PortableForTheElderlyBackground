package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.user;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface userMapper extends BaseMapper<user> {
}
