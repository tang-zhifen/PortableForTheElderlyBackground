package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.science.scienceImages;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface scienceImagesMapper extends BaseMapper<scienceImages> {
}
