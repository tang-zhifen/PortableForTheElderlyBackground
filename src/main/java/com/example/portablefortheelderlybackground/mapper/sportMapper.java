package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.sport.sport;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface sportMapper extends BaseMapper<sport> {
}
