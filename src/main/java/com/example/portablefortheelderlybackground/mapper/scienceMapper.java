package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.science.science;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface scienceMapper extends BaseMapper<science> {
}
