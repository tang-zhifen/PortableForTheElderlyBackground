package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.shop.shop;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface shopMapper extends BaseMapper<shop> {
}
