package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.dish.dishUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface dishUserMapper extends BaseMapper<dishUser> {
}
