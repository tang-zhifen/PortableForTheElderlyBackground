package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.rate.rate;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface rateMapper extends BaseMapper<rate> {
}
