package com.example.portablefortheelderlybackground.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.portablefortheelderlybackground.pojo.question.survey;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface surveyMapper extends BaseMapper<survey> {
}
