package com.example.portablefortheelderlybackground.vo.answerVo;

import com.example.portablefortheelderlybackground.pojo.question.answer;
import lombok.Data;

import java.io.Serial;

@Data
public class answerVo extends answer {
    @Serial
    private static final long serialVersionUID = -4649605957916315950L;
}
