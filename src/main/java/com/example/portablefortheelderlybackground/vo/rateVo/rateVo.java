package com.example.portablefortheelderlybackground.vo.rateVo;

import com.example.portablefortheelderlybackground.pojo.rate.rate;

import java.io.Serial;

public class rateVo extends rate {
    @Serial
    private static final long serialVersionUID = -8991246616948881637L;

    public rateVo() {

    }

    public rateVo(rate vo) {
        this.setCreateTime(vo.getCreateTime());
        this.setType(vo.getType());
        this.setId(vo.getId());
        this.setValue(vo.getValue());
        this.setUpdateTime(vo.getUpdateTime());
    }
}
