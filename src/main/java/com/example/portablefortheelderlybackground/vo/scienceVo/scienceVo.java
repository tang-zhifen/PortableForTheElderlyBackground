package com.example.portablefortheelderlybackground.vo.scienceVo;

import com.example.portablefortheelderlybackground.pojo.science.science;
import com.example.portablefortheelderlybackground.pojo.science.scienceImages;
import lombok.Data;

import java.io.Serial;
import java.util.List;

@Data
public class scienceVo extends science {
    @Serial
    private static final long serialVersionUID = 3537343981752736887L;
    private List<scienceImages> scienceImagesList;
}
