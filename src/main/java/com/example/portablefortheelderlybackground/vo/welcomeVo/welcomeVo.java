package com.example.portablefortheelderlybackground.vo.welcomeVo;

import com.example.portablefortheelderlybackground.pojo.welcome.welcome;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Data
public class welcomeVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 4559324227098052658L;
    List<welcome> list;

}
