package com.example.portablefortheelderlybackground.vo.historyVo;

import com.example.portablefortheelderlybackground.pojo.science.science;
import lombok.Data;

import java.io.Serial;
import java.time.LocalDateTime;

@Data
public class lookAtScienceViewVo extends science {
    @Serial
    private static final long serialVersionUID = -5949254896454403558L;
    private String lookTime;
    private String images;

}
