package com.example.portablefortheelderlybackground.vo.historyVo;

import com.example.portablefortheelderlybackground.dto.scienceDto.scienceDto;
import com.example.portablefortheelderlybackground.pojo.history.lookHistory;
import lombok.Data;
/**
 * @description: 用于组装science的lookHistoryVo
 * @author 86187
 * @date: 2023/10/16 22:58
 */

import javax.validation.constraints.NotNull;
import java.io.Serial;

@Data
public class lookAtScienceVo extends lookHistory {
    @Serial
    private static final long serialVersionUID = 6430082331419665408L;
}
