package com.example.portablefortheelderlybackground.vo.questionVo;

import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.pojo.question.answer;
import com.example.portablefortheelderlybackground.pojo.question.question;
import com.example.portablefortheelderlybackground.vo.Vo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serial;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author 86187
 * @description: 每一道问题的vo对象
 * @date: 2023/7/6 11:09
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class topicVo extends question implements Vo {
    @Serial
    private static final long serialVersionUID = 1592331559444398296L;
    @NotEmpty(message = "必须拥有答案", groups = {AddGroup.class})
    private List<answer> answerList;
}
