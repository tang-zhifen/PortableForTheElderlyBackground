package com.example.portablefortheelderlybackground.vo.questionVo;

import com.example.portablefortheelderlybackground.pojo.question.survey;
import com.example.portablefortheelderlybackground.vo.Vo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.util.List;

/**
 * @author 86187
 * @description: 调查报告的vo对象
 * @date: 2023/7/6 11:10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class surveyVo extends survey implements Vo {

    @Serial
    private static final long serialVersionUID = 1472594771132881565L;


    private List<topicVo> topicVoList;
}
