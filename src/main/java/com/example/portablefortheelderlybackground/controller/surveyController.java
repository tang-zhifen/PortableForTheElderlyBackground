package com.example.portablefortheelderlybackground.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.portablefortheelderlybackground.sevice.surveyService;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.questionVo.surveyVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;

/**
 * @author 86187
 * @description: 调查报告控制器
 * @date: 2023/7/7 11:20
 */

@RestController
@RequestMapping("/survey")
@Api(tags = {"调查报告控制器"})
@Slf4j
@Validated
public class surveyController {
    private final
    surveyService surveyService;

    //构造器注入
    @Autowired
    public surveyController(surveyService surveyService) {
        this.surveyService = surveyService;
    }

    @GetMapping("/query/{id}")
    @ApiOperation("获取survey")
    public Response<IPage<surveyVo>> getSurveyById(@Pattern(regexp = "^\\d+$", message = "id为纯数字") @PathVariable String id) {
        IPage<surveyVo> surveyVoIPage = surveyService.getSurveyById(id);
        return Response.status(surveyVoIPage);
    }

    @PostMapping("/save")
    @ApiOperation("增加survey参与的人数")
    @PreAuthorize("hasAnyAuthority('system:query')")//拥有query权限即可,即需要登录
    public Response<Boolean> saveSurveyVoPeoples(@RequestBody @Validated surveyVo vo) {
        return Response.status(surveyService.saveSurveyVoPeoples(vo));
    }

    @GetMapping("/query/new")
    @ApiOperation("获取需要的调查问卷")
    public Response<IPage<surveyVo>> getNewSurvey() {
        return Response.status(surveyService.getSurveyByCurrent());
    }
}
