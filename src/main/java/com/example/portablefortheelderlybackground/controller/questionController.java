package com.example.portablefortheelderlybackground.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.sevice.questionService;
import com.example.portablefortheelderlybackground.utils.PageUtil.PageGenerateUtil;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.questionVo.topicVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import static com.example.portablefortheelderlybackground.Enum.Message.ReturnMessageEnum.*;

/**
 * @author 86187
 * @description: 问题的控制器
 * @date: 2023/7/6 11:17
 */

@RestController
@Validated//开启参数校验
@RequestMapping("/question")
@Api(tags = {"调查问卷问题"})
public class questionController {

    private final questionService questionService;

    //选择用构造器注入
    @Autowired
    public questionController(questionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/query")
    @ApiOperation(value = "获取所有问题及其答案")
    public Response<IPage<topicVo>> getAllQuestion(@RequestParam(value = "type", defaultValue = "自制") String type) {
        IPage<topicVo> allTopicVo = questionService.getAllTopicVo(PageGenerateUtil.PageVoGenerate(new topicVo()), type);
        return Response.status(allTopicVo, SUCCESS.getCode(), SUCCESS.getMessage());
    }

    @GetMapping("/query/{id}")
    @ApiOperation(value = "根据id获取问题及其答案")
    public Response<topicVo> getQuestionById(@NotBlank(message = "id不能为空") @PathVariable String id) {
        topicVo topicVoById = questionService.getTopicVoById(id);
        return Response.status(topicVoById, SUCCESS.getCode(), SUCCESS.getMessage());
    }

    @DeleteMapping("/delete")
    @ApiOperation(value = "根据ids删除问题及其答案")
    @PreAuthorize("hasAnyAuthority('Admin:delete','Admin:root')")
    public Response<Boolean> deleteQuestionsByIds(@NotEmpty(message = "ids数组不能为空") @RequestBody String[] ids) {
        return questionService.deleteQuestionsBatch(ids);
    }

    @PostMapping("/save")
    @ApiOperation(value = "添加问题")
    @PreAuthorize("hasAnyAuthority('Admin:insert','Admin:root')")
    public Response<topicVo> addQuestion(@Validated(value = {AddGroup.class}) @RequestBody topicVo topicVo) {
        return questionService.addQuestion(topicVo);
    }
}
