package com.example.portablefortheelderlybackground.controller;

import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.pojo.rate.userSuggestion;
import com.example.portablefortheelderlybackground.sevice.userSuggestionService;
import com.example.portablefortheelderlybackground.utils.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/suggestion")
@Api(tags = "用户建议的控制器")
@Validated
public class suggestionController {

    private final userSuggestionService userSuggestionService;

    public suggestionController(userSuggestionService userSuggestionService) {
        this.userSuggestionService = userSuggestionService;
    }

    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('system:insert')")
    @ApiOperation("保存用户建议")
    public Response<Boolean> saveSuggestion(@Validated(value = {AddGroup.class}) @RequestBody userSuggestion userSuggestion) {
        return userSuggestionService.saveUserSuggestion(userSuggestion);
    }
}