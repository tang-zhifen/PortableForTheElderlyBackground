package com.example.portablefortheelderlybackground.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.dto.scienceDto.scienceDto;
import com.example.portablefortheelderlybackground.sevice.scienceService;
import com.example.portablefortheelderlybackground.utils.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;


@RestController
@Slf4j
@RequestMapping("/science")
@Transactional
@Api(tags = "科普文章的控制器")
@Validated
public class scienceController {
    private final
    scienceService scienceService;

    @Autowired
    public scienceController(scienceService scienceService) {
        this.scienceService = scienceService;
    }

    /**
     * @return com.example.portablefortheelderlybackground.utils.R
     * @description: 查询对应类型的科普消息, 用于列表展示, 返回的数据为List<science>,数据做分页
     * @author 86187
     * @date: 2023/4/1 19:31
     */
    @GetMapping("/query")
    @ApiOperation("查询所用科普")
    public Response<Page<scienceDto>> getAllScienceMessage(@RequestParam(value = "pages", defaultValue = "1") Integer pages, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(required = false, defaultValue = "原创") String type) {
        return scienceService.getAllScienceMessage(pages, pageSize, type, null);
    }

    /**
     * @param id id
     * @return com.example.portablefortheelderlybackground.utils.R
     * @description: 根据id查询数据，用户点击过程
     * @author 86187
     * @date: 2023/4/2 14:42
     */

    @GetMapping("/query/{id}")
    @ApiOperation("根据id获取科普文章")
    public Response<scienceDto> getScienceById(@Pattern(regexp = "^\\d+$", message = "文章id只包含数字") @PathVariable String id) {
        return scienceService.getScienceById(id);
    }

    /**
     * @param scienceDto scienceDto
     * @return com.example.portablefortheelderlybackground.utils.R
     * @description: 保存科普文章
     * @author 86187
     * @date: 2023/4/2 21:28
     */

    @PostMapping("/save")
    @ApiOperation(value = "保存科普文章", notes = "需要insert的权限")
    @PreAuthorize("hasAnyAuthority('Admin:insert','Admin:root')")
    public Response saveScienceMessage(@Validated(value = {AddGroup.class}) @RequestBody scienceDto scienceDto) {
        return scienceService.saveScienceMessage(scienceDto);
    }

    //搜索功能,根据标题进行搜索

    /**
     * @param title    标题
     * @param pages    页数
     * @param pageSize 每页大小
     * @return com.example.portablefortheelderlybackground.utils.R<com.baomidou.mybatisplus.extension.plugins.pagination.Page < com.example.portablefortheelderlybackground.dto.scienceDto>>
     * @description: 根据前端传入的title模糊查询数据库
     * @author 86187
     * @date: 2023/6/23 22:30
     */

    @GetMapping("/search")
    @ApiOperation(value = "根据文章标题模糊查询")
    public Response<Page<scienceDto>> getScienceByTitle(@RequestParam(defaultValue = "如何") String title, @RequestParam(defaultValue = "1") Integer pages, @RequestParam(defaultValue = "10") Integer pageSize) {
        return scienceService.getScienceByTitle(title, pages, pageSize);
    }

}
