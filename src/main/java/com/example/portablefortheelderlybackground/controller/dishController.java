package com.example.portablefortheelderlybackground.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.portablefortheelderlybackground.config.group.UpdateGroup;
import com.example.portablefortheelderlybackground.pojo.dish.dish;
import com.example.portablefortheelderlybackground.sevice.dishService;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.dishVo.dishVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;


@RestController
@RequestMapping("/dish")
@Api(tags = {"水果蔬菜控制器"})
@Validated
@Valid
public class dishController {
    private final dishService dishService;

    //这里选择采用构造器注入
    @Autowired
    public dishController(dishService dishService) {
        this.dishService = dishService;
    }


    /**
     * @return com.example.portablefortheelderlybackground.utils.R<com.baomidou.mybatisplus.extension.plugins.pagination.Page < com.example.portablefortheelderlybackground.pojo.dish>>
     * @description: 查询全部dish, 用于列表展示
     * @author 86187
     * @date: 2023/4/5 12:46
     */

    @GetMapping("/query")
    @ApiOperation("查询:所有的dish")
    public Response<Page<dish>> getDishList(@RequestParam(value = "pages", defaultValue = "1") Integer pages, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "type", required = false, defaultValue = "蔬菜水果") String type) {
        return dishService.getDishList(pages, pageSize, type);
    }

    /**
     * @param id dish的id
     * @return com.example.portablefortheelderlybackground.utils.R<com.example.portablefortheelderlybackground.pojo.dish>
     * @description: 查询单个dish，用于具体展示
     * @author 86187
     * @date: 2023/4/5 12:39
     */

    @GetMapping("/query/{id}")
    @ApiOperation("查询:根据id查询dish")
    public Response<dishVo> getDishById(@Pattern(regexp = "^\\d+$", message = "id不合法") @PathVariable String id) {
        //根据id查询
        return dishService.getDishById(id);
    }

    /**
     * @param updateDishVo 修改的Vo
     * @return com.example.portablefortheelderlybackground.utils.R<com.example.portablefortheelderlybackground.pojo.dish>
     * @description: 修改dish数据, 用于后台,更新redis缓存
     * @author 86187
     * @date: 2023/4/5 12:55
     */

    @PutMapping("/update")
    @PreAuthorize("hasAnyAuthority('Admin:update','Admin:root')")
    @ApiOperation("修改:根据id修改")
    public Response<dishVo> updateDishById(@Validated(value = {UpdateGroup.class}) @RequestBody dishVo updateDishVo) {
        return dishService.updateDishById(updateDishVo);
    }

    /**
     * @param saveDish 保存的dish
     * @return com.example.portablefortheelderlybackground.utils.R
     * @description: 保存dish, 后台api,更新redis缓存
     * @author 86187
     * @date: 2023/5/22 8:55
     */


    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('Admin:insert','Admin:root')")
    @ApiOperation("添加:添加dish")
    public Response<dishVo> saveDish(@Validated @RequestBody dishVo saveDish) {
        return dishService.saveDish(saveDish);
    }

    /**
     * @param id
     * @return com.example.portablefortheelderlybackground.utils.R
     * @description: 删除dish, 清楚redis缓存,
     * @author 86187
     * @date: 2023/5/22 8:50
     */

    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAnyAuthority('Admin:delete','Admin:root')")
    @ApiOperation("删除:根据id删除dish")
    public Response<Boolean> deleteDish(@Pattern(regexp = "^\\d+$", message = "id不合法") @PathVariable String id) {
        return dishService.deleteDish(id);
    }
}
