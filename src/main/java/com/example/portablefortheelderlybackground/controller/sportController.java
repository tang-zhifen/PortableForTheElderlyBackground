package com.example.portablefortheelderlybackground.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.config.group.UpdateGroup;
import com.example.portablefortheelderlybackground.pojo.sport.sport;
import com.example.portablefortheelderlybackground.sevice.sportService;
import com.example.portablefortheelderlybackground.utils.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;

@RestController
@Slf4j
@RequestMapping("/sport")
@Transactional
@Api(tags = {"运动类别控制器"})
@Validated
public class sportController {
    private final sportService sportService;

    //构造器注入
    @Autowired
    public sportController(sportService sportService) {
        this.sportService = sportService;
    }

    /**
     * @return com.example.portablefortheelderlybackground.utils.R<com.baomidou.mybatisplus.extension.plugins.pagination.Page < com.example.portablefortheelderlybackground.pojo.sport>>
     * @description: 查询全部sport, 用于列表展示
     * @author 86187
     * @date: 2023/4/5 12:59
     */


    @GetMapping("/query")
    @ApiOperation(value = "获取运动列表:根据类型")
    public Response<Page<sport>> getSportList(@RequestParam(value = "pages", defaultValue = "1") Integer pages, @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize, @RequestParam(value = "type", required = false, defaultValue = "跑步") String type) {
        return sportService.getSportList(pages, pageSize, type);
    }

    /**
     * @param id id
     * @return com.example.portablefortheelderlybackground.utils.R<com.example.portablefortheelderlybackground.pojo.sport>
     * @description: 查询单个dish，用于具体展示
     * @author 86187
     * @date: 2023/4/5 13:00
     */


    @GetMapping("/query/{id}")
    @ApiOperation(value = "获取运动列表:根据id")
    public Response<sport> getSportById(@Pattern(regexp = "[0-9]{1,20}") @PathVariable String id) {
        return sportService.getSportById(id);
    }

    /**
     * @param updateSport 修改实体类
     * @return com.example.portablefortheelderlybackground.utils.R<com.example.portablefortheelderlybackground.pojo.dish>
     * @description: 修改dish数据, 用于后台
     * @author 86187
     * @date: 2023/4/5 12:55
     */

    @PutMapping("/update")
    @ApiOperation(value = "修改运动信息:根据id")
    @PreAuthorize("hasAnyAuthority('Admin:update','Admin:root')")
    public Response<sport> updateSportById(@Validated(value = {UpdateGroup.class}) @RequestBody sport updateSport) {
        return sportService.updateSportById(updateSport);
    }

    /**
     * @param saveSport 保存实体类
     * @return com.example.portablefortheelderlybackground.utils.R
     * @description: 保存sport, 后台api
     * @author 86187
     * @date: 2023/4/5 13:26
     */

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('system:insert')")
    @ApiOperation(value = "保存运动")
    public Response saveSport(@Validated(value = {AddGroup.class}) @RequestBody sport saveSport) {
        return sportService.saveSport(saveSport);
    }

    /**
     * @param id id
     * @return com.example.portablefortheelderlybackground.utils.R
     * @description: 删除dish，清楚redis缓存
     * @author 86187
     * @date: 2023/4/5 13:33
     */
    @DeleteMapping("/delete/{id}")
    @PreAuthorize("hasAnyAuthority('Admin:delete','Admin:root')")
    @ApiOperation(value = "删除运动:根据id")
    public Response deleteSport(@Pattern(regexp = "[0-9]{18,20}") @PathVariable String id) {
        return sportService.deleteSport(id);
    }
}
