package com.example.portablefortheelderlybackground.controller;

import com.example.portablefortheelderlybackground.Enum.Message.ReturnMessageEnum;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.config.group.UpdateGroup;
import com.example.portablefortheelderlybackground.config.validation.isPhone;
import com.example.portablefortheelderlybackground.pojo.user;
import com.example.portablefortheelderlybackground.sevice.userService;
import com.example.portablefortheelderlybackground.utils.Response;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.Map;


@RestController
@Slf4j
@RequestMapping("/user")
@Transactional
@Api(tags = "用户登录注册控制器")
@Validated //开启数据校验
public class userController {
    //    注入userService
    private final userService userService;

    @Autowired
    public userController(userService userService) {
        this.userService = userService;
    }

    /**
     * @param userVo 用户主体
     * @return String
     * @description: 用户注册接口
     * @author 86187
     * @date: 2023/4/1 16:40
     */
    @PostMapping("/signIn")
    @ApiOperation(value = "用户注册:可使用手机号注册")
    public Response<user> signIn(@Validated(value = {AddGroup.class}) @RequestBody user userVo, @Pattern(regexp = "^\\d{6}$") String code) {
        return userService.signIn(userVo, code);
    }


    @GetMapping("/phoneSignIn")
    @ApiOperation(value = "用户:发送手机验证码")
    public Response<Boolean> PhoneSignIn(@isPhone @RequestParam(value = "phone") String phone) {
        return Response.status(userService.sendMessageByPhone(phone), ReturnMessageEnum.PHONE_CODE.getCode(), ReturnMessageEnum.PHONE_CODE.getMessage());
    }

    @GetMapping("/phoneLogin")
    @ApiOperation(value = "用户:手机验证码登录")
    public Response<Map<String, Object>> loginByPhoneCode(@isPhone String phone, @Pattern(regexp = "^\\d{6}") @RequestParam(value = "code") String code) {
        return userService.loginByPhoneCode(phone, code);
    }

    /**
     * @param name     用户名
     * @param password 用户密码
     * @return com.example.portablefortheelderlybackground.utils.R
     * @description: 登录进行认证
     * @author 86187
     * @date: 2023/5/19 12:29
     */


    @GetMapping("/login")
    @ApiOperation(value = "用户:密码登录")
    public Response<Map<String, Object>> loginIn(@NotBlank(message = "姓名不能为空") @RequestParam(value = "name") String name, @NotBlank(message = "密码不能为空") @RequestParam(value = "password") String password) {
        return userService.loginIn(name, password);
    }

    /**
     * @param user 用户主体
     * @return com.example.portablefortheelderlybackground.utils.R<java.lang.String>
     * @description: 用户修改信息, 同时进行权限校验
     * @author 86187
     * @date: 2023/5/19 23:19
     */

    @PutMapping("/update")
    @ApiOperation("用户更改信息")
    @PreAuthorize("hasAnyAuthority('system:update')")//进行权限校验
    public Response<String> updateUserInfo(@Validated({UpdateGroup.class}) @RequestBody user user) {
        return userService.updateUserInfo(user);
    }

    /**
     * @param refreshToken refreshToken 接受用户的刷线token
     * @return com.example.portablefortheelderlybackground.utils.R<java.lang.String>
     * @description: 用户刷新token
     * @author 86187
     * @date: 2023/9/22 19:57
     */

    @GetMapping("/refreshToken")
    @ApiOperation("用户刷新token")
    @PreAuthorize("hasAnyAuthority('system:query')")
    public Response<Map<String, Object>> refreshTokenMethod(@RequestParam(value = "refreshToken") String refreshToken) {
        return userService.userRefreshToken(refreshToken);
    }
}
