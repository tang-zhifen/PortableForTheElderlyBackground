package com.example.portablefortheelderlybackground.controller;

import com.example.portablefortheelderlybackground.sevice.orderService;
import com.example.portablefortheelderlybackground.utils.Response;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Pattern;

@RestController
@Slf4j
@RequestMapping("/order")
@Api(tags = {"订单控制器"})
public class OrderController {
    private final orderService orderService;

    public OrderController(orderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/cm")
    public Response<Boolean> createOrder(@RequestParam(value = "id") @Pattern(regexp = "^\\d+$", message = "id只能包含数字") String id) {
        return Response.status(orderService.createOrder(id));
    }
}
