package com.example.portablefortheelderlybackground.controller.history;

import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.sevice.lookHistoryService;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.historyVo.lookAtScienceViewVo;
import com.example.portablefortheelderlybackground.vo.historyVo.lookAtScienceVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@RequestMapping("/history/science")
@Api(tags = {"科普文章同步缓存的控制器"})
@Validated
public class scienceHistoryController {
    private final lookHistoryService lookHistoryService;

    @Autowired
    public scienceHistoryController(lookHistoryService lookHistoryService) {
        this.lookHistoryService = lookHistoryService;
    }

    @PostMapping("/save")
    @PreAuthorize("hasAnyAuthority('system:insert')")
    @ApiOperation(value = "同步历史记录")
    public Response<String> saveUserScienceHistory(@Validated @RequestBody List<lookAtScienceVo> lookAtScienceVoList) {
        return lookHistoryService.saveScienceHistory(lookAtScienceVoList);
    }

    @GetMapping("/query/{userId}")
    @ApiOperation(value = "查询用户记录", notes = "只同步最近的20条")
    public Response<List<lookAtScienceViewVo>> queryUserScienceHistory(@PathVariable @NotBlank String userId, @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer pageSize) {
        return lookHistoryService.queryScienceHistoryByUserId(userId, page, pageSize);
    }

    @DeleteMapping("/delete/{userId}")
    @ApiOperation(value = "删除用户观看记录", notes = "支持批量删除")
    @PreAuthorize("hasAnyAuthority('system:delete','Admin:root')")
    public Response<Boolean> deleteUserScienceHistory(@PathVariable @NotBlank String userId, @NotEmpty @RequestBody String[] scienceIds) {
        return lookHistoryService.deleteScienceHistory(userId, scienceIds);
    }
}
