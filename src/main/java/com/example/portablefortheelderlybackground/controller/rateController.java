package com.example.portablefortheelderlybackground.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.sevice.rateService;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.rateVo.rateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

/**
 * @author 86187
 * @description: 评分控制器
 * @date: 2023/7/8 16:22
 */
@RestController
@Slf4j
@RequestMapping("/rate")
@Api(tags = "用户评分的控制器")
@Validated
public class rateController {

    private final
    rateService rateService;

    //按照构造器注入
    @Autowired
    public rateController(rateService rateService) {
        this.rateService = rateService;
    }

    @GetMapping("/query")
    @ApiOperation("获取所有的评分数据")
    @PreAuthorize("hasAuthority('system:query')")
    public Response<Page<rateVo>> getAllRateVo(@RequestParam(value = "type", required = false) @NotBlank(message = "type不能为空") String type,
                                               @RequestParam(value = "pages", defaultValue = "1") Integer pages,
                                               @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) {
        return rateService.getAllRateVo(type, pages, pageSize);
    }

    @PostMapping("/save")
    @ApiOperation("保存用户评分")
    @PreAuthorize("hasAnyAuthority('system:insert','Admin:root')")
    public Response<Boolean> saveRateVo(@Validated(value = {AddGroup.class}) @RequestBody rateVo vo) {
        return rateService.saveRateVo(vo);
    }

    @DeleteMapping("/delete")
    @ApiOperation("批量删除用户评分")
    @PreAuthorize("hasAnyAuthority('Admin:delete','Admin:root')")
    public Response<Boolean> deleteRateVoByIds(@NotEmpty(message = "ids数组不能为空") @RequestBody String[] ids) {
        return rateService.deleteRateVoByIds(ids);
    }
}
