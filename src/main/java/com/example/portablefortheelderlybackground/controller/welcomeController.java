package com.example.portablefortheelderlybackground.controller;

import com.example.portablefortheelderlybackground.sevice.welcomeService;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.welcomeVo.welcomeVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@RestController
@RequestMapping("/welcome")
@Slf4j
@Api(tags = {"欢迎首页设置"})
@Validated
public class welcomeController {

    //依赖注入welcomeService
    private final welcomeService welcomeService;
    @Autowired
    public welcomeController(welcomeService welcomeService) {
        this.welcomeService = welcomeService;
    }


    @GetMapping("/query")
    @ApiOperation("获取轮播图:根据类型")
    public Response<welcomeVo> getSwiperByType(@NotBlank(message = "类型不空") @RequestParam String type) {
        return welcomeService.getSwiperByType(type);
    }

    @DeleteMapping("/delete")
    @ApiOperation("删除轮播图")
    @PreAuthorize("hasAnyAuthority('Admin:delete','Admin:root')")
    public Response<Boolean> deleteSwiperByIds(@NotEmpty(message = "ids数组不能为空") @RequestBody String[] ids) {
        return welcomeService.deleteSwiperByIds(ids);
    }
}
