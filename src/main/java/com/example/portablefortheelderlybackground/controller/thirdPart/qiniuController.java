package com.example.portablefortheelderlybackground.controller.thirdPart;

import com.example.portablefortheelderlybackground.common.Message.QiniuMessage;
import com.example.portablefortheelderlybackground.config.Qiniu.uploadUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@Validated
@RequestMapping("/api/uptoken")
@Api(tags = {"七牛云图片存储"})
public class qiniuController {

    private final uploadUtil uploadUtil;

    //采用构造器注入
    public qiniuController(uploadUtil uploadUtil) {
        this.uploadUtil = uploadUtil;
    }

    /**
     * @return java.util.Map<java.lang.String, java.lang.String>
     * @description: 只生成项目oss空间的token
     * @author 86187
     * @date: 2023/9/3 12:27
     */
    @GetMapping()
    @ApiOperation(value = "生成项目token")
    public Map<String, String> getToken() {
        String token = uploadUtil.uploadFile();
        Map<String, String> map = new HashMap<>();
        map.put("uptoken", token);
        System.out.println(token);
        return map;
    }

    /**
     * @param accessKey 公钥
     * @param secretKey 私钥
     * @param bucket    空间名称
     * @return java.util.Map<java.lang.String, java.lang.String>
     * @description: 提供对外的接口，生产简单的token
     * @author 86187
     * @date: 2023/9/3 12:28
     */

    @GetMapping("/self")
    @ApiOperation(value = "根据传入的信息生成token")
    public Map<String, String> getSelfToken(@RequestParam(required = true) String accessKey, @RequestParam(required = true) String secretKey, @RequestParam(required = true) String bucket) {
        Map<String, String> result;
        String token = uploadUtil.uploadFileSelfToken(accessKey, secretKey, bucket);
        Map<String, String> map = new HashMap<>();
        if (StringUtils.hasText(token)) {
            map.put("error", QiniuMessage.TOKEN_GENERATE_ERROR);
        } else {
            map.put("uptoken", token);
        }
        result = map;
        return result;
    }
}
