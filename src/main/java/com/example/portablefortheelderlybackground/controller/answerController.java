package com.example.portablefortheelderlybackground.controller;

import com.example.portablefortheelderlybackground.Enum.Message.ReturnMessageEnum;
import com.example.portablefortheelderlybackground.config.group.AddGroup;
import com.example.portablefortheelderlybackground.pojo.question.answer;
import com.example.portablefortheelderlybackground.sevice.answerService;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.answerVo.answerVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @author 86187
 * @description: 问题答案控制器
 * @date: 2023/7/6 19:17
 */

@RestController
@RequestMapping("/answer")
@Api(tags = {"问题答案控制器"})
@Validated
public class answerController {
    private final answerService answerService;

    //选择构造器注入
    @Autowired
    public answerController(answerService answerService) {
        this.answerService = answerService;
    }

    @GetMapping("/query/{id}")
    @ApiOperation(value = "根据问题id获取answer")
    public Response<List<answer>> getAnswerByQuestionId(@Pattern(regexp = "^\\d+$", message = "请传入合法的id") @PathVariable String id) {
        return Response.status(answerService.getAnswerByQuestionId(id), ReturnMessageEnum.SUCCESS.getCode(), ReturnMessageEnum.SUCCESS.getMessage());
    }

    @DeleteMapping("/delete")
    @ApiOperation(value = "批量删除answer")
    @PreAuthorize("hasAnyAuthority('Admin:delete','Admin:root')")
    public Response<Boolean> deleteAnswerByQuestionId(@NotEmpty(message = "ids数组不能为空") @RequestBody String[] ids) {
        return Response.status(answerService.deleteAnswerByQuestionId(ids), ReturnMessageEnum.DELETE_SUCCESS.getCode(), ReturnMessageEnum.DELETE_SUCCESS.getMessage());
    }

    @PostMapping("/save/{id}")
    @ApiOperation(value = "添加answer:根据id")
    @PreAuthorize("hasAnyAuthority('Admin:insert','Admin:root')")
    public Response<Boolean> saveAnswerById(@Pattern(regexp = "^\\d+$", message = "id不合法") @PathVariable String id, @Validated(value = {AddGroup.class}) @RequestBody answerVo answer) {
        return Response.status(answerService.saveAnswer(id, answer), ReturnMessageEnum.SUCCESS.getCode(), ReturnMessageEnum.SUCCESS.getMessage());
    }
}
