package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.rate.rate;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.rateVo.rateVo;


public interface rateService extends IService<rate> {
    Response<Page<rateVo>> getAllRateVo(String type, Integer pages, Integer pageSize);

    Response<Boolean> saveRateVo(rateVo vo);

    Response<Boolean> deleteRateVoByIds(String[] ids);
}
