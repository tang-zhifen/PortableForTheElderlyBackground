package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.user;
import com.example.portablefortheelderlybackground.utils.Response;

import java.util.Map;

public interface userService extends IService<user> {
    Response<String> updateUserInfo(user user);

    Response<Map<String, Object>> loginIn(String name, String password);

    Response<user> signIn(user user, String code);

    boolean sendMessageByPhone(String... phone);

    Response<Map<String, Object>> loginByPhoneCode(String phone, String code);

    Response<Map<String, Object>> userRefreshToken(String token);
}
