package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.shopMapper;
import com.example.portablefortheelderlybackground.pojo.shop.shop;
import com.example.portablefortheelderlybackground.sevice.shopService;
import org.springframework.stereotype.Service;

@Service
public class shopServiceImpl extends ServiceImpl<shopMapper, shop> implements shopService {
}
