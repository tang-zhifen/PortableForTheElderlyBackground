package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.roleMapper;
import com.example.portablefortheelderlybackground.pojo.RABC.role;
import com.example.portablefortheelderlybackground.sevice.roleService;
import org.springframework.stereotype.Service;

@Service
public class roleServiceImpl extends ServiceImpl<roleMapper, role> implements roleService {
}
