package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.scienceImagesMapper;
import com.example.portablefortheelderlybackground.pojo.science.scienceImages;
import com.example.portablefortheelderlybackground.sevice.scienceImagesService;
import org.springframework.stereotype.Service;

@Service
public class scienceImagesServiceImpl extends ServiceImpl<scienceImagesMapper, scienceImages> implements scienceImagesService {
}
