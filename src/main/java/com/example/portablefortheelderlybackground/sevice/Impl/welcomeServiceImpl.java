package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.common.Message.ResultMessage;
import com.example.portablefortheelderlybackground.common.StringCommon;
import com.example.portablefortheelderlybackground.common.key.paramsKey;
import com.example.portablefortheelderlybackground.common.redis.redisKey;
import com.example.portablefortheelderlybackground.mapper.welcomeMapper;
import com.example.portablefortheelderlybackground.pojo.welcome.welcome;
import com.example.portablefortheelderlybackground.sevice.welcomeService;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.utils.VoUtil.welcomeVoSingleton;
import com.example.portablefortheelderlybackground.vo.welcomeVo.welcomeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class welcomeServiceImpl extends ServiceImpl<welcomeMapper, welcome> implements welcomeService {

    private final
    RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public welcomeServiceImpl(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * @param type 图片类型
     * @return com.example.portablefortheelderlybackground.utils.R<com.example.portablefortheelderlybackground.vo.welcomeVo.welcomeVo>
     * @description: 根据类型获取图片
     * @author 86187
     * @date: 2023/7/15 20:14
     */

    @Override
    public Response<welcomeVo> getSwiperByType(String type) {
        //先判断redis是否存在
        welcomeVo redisWelcomeVo = (welcomeVo) redisTemplate.opsForValue().get(redisKey.welcomeSwiperKey + type);
        if (!ObjectUtils.isEmpty(redisWelcomeVo)) {
            return Response.status(redisWelcomeVo);
        }
        LambdaQueryWrapper<welcome> queryWrapper = new LambdaQueryWrapper<>();
        //这里不需要对type进行校验,因为在控制层已经做了参数校验
        queryWrapper.eq(welcome::getType, type)
                .eq(welcome::getStatus, paramsKey.SHOW_STATUS)
                .eq(welcome::getRemove, paramsKey.NOT_DELETE);
        //baseMapper查询
        List<welcome> welcomes = baseMapper.selectList(queryWrapper);
        //如果查询结果不为空
        welcomeVo instance = welcomeVoSingleton.getInstance();
        if (!ObjectUtils.isEmpty(welcomes)) {
            instance.setList(welcomes);
            //存redis缓存
            redisTemplate.opsForValue().set(redisKey.welcomeSwiperKey + type, instance, StringCommon.TimeOut, TimeUnit.HOURS);
        }
        return Response.status(instance, ResultMessage.success_message);
    }

    /**
     * @param ids id的String[]数组
     * @return com.example.portablefortheelderlybackground.utils.R<java.lang.Boolean>
     * @description: 删除图片根据ids
     * @author 86187
     * @date: 2023/7/15 20:15
     */

    @Override
    public Response<Boolean> deleteSwiperByIds(String[] ids) {
        //todo 轮播图不考虑数据一致性
        welcome WelcomeBuilder = new welcome().setIsDelete(paramsKey.IS_DELETE);
        LambdaQueryWrapper<welcome> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(welcome::getId, ids);
        int update = baseMapper.update(WelcomeBuilder, queryWrapper);
        return Response.status(update > paramsKey.NUM_ZERO);
    }
}
