package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.Enum.type.suggestionType;
import com.example.portablefortheelderlybackground.common.key.paramsKey;
import com.example.portablefortheelderlybackground.mapper.userSuggestionMapper;
import com.example.portablefortheelderlybackground.pojo.rate.userSuggestion;
import com.example.portablefortheelderlybackground.pojo.userDetailImpl;
import com.example.portablefortheelderlybackground.sevice.userSuggestionService;
import com.example.portablefortheelderlybackground.utils.Response;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import static com.example.portablefortheelderlybackground.Enum.Message.ReturnMessageEnum.*;

@Service
public class userSuggestionServiceImpl extends ServiceImpl<userSuggestionMapper, userSuggestion> implements userSuggestionService {
    @Override
    public Response<Boolean> saveUserSuggestion(userSuggestion userSuggestion) {
        //1.从上下文获取用户认证
        userDetailImpl principal = (userDetailImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //2.获取用户id,判空
        if (ObjectUtils.isEmpty(principal)) {
            return Response.error(false, SAVE_ERROR.getCode(), SAVE_ERROR.getMessage());
        }
        userSuggestion.setUserId(principal.getUser().getId());
        //3.判断用户建议类型,如果为空则赋予默认积极类型
        if (!StringUtils.hasLength(userSuggestion.getType())) {
            userSuggestion.setType(suggestionType.POSITIVE.getTYPE());
        }
        //4.进行插入
        int insert = baseMapper.insert(userSuggestion);
        if (insert > paramsKey.NUM_ZERO) {
            return Response.success(true, SAVE_SUCCESS.getCode(), SAVE_SUCCESS.getMessage());
        }
        return Response.error(false, SAVE_ERROR.getCode(), SAVE_ERROR.getMessage());
    }
}
