package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.weatherDataMapper;
import com.example.portablefortheelderlybackground.pojo.weather.weatherData;
import com.example.portablefortheelderlybackground.sevice.weatherDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class weatherDataServiceImpl extends ServiceImpl<weatherDataMapper, weatherData> implements weatherDataService {
}
