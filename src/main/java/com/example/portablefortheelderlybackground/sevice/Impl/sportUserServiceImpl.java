package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.sportUserMapper;
import com.example.portablefortheelderlybackground.pojo.sport.sportUser;
import com.example.portablefortheelderlybackground.sevice.sportUserService;
import org.springframework.stereotype.Service;

@Service
public class sportUserServiceImpl extends ServiceImpl<sportUserMapper, sportUser> implements sportUserService {
}
