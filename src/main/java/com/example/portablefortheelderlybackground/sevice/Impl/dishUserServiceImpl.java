package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.dishUserMapper;
import com.example.portablefortheelderlybackground.pojo.dish.dishUser;
import com.example.portablefortheelderlybackground.sevice.dishUserService;
import org.springframework.stereotype.Service;

@Service
public class dishUserServiceImpl extends ServiceImpl<dishUserMapper, dishUser> implements dishUserService {
}
