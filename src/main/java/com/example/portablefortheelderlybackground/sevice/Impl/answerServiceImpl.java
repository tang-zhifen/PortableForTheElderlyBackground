package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.common.StringCommon;
import com.example.portablefortheelderlybackground.common.key.paramsKey;
import com.example.portablefortheelderlybackground.common.redis.redisKey;
import com.example.portablefortheelderlybackground.mapper.answerMapper;
import com.example.portablefortheelderlybackground.pojo.question.answer;
import com.example.portablefortheelderlybackground.sevice.answerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@Transactional(timeout = 5)
public class answerServiceImpl extends ServiceImpl<answerMapper, answer> implements answerService {

    private final
    RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public answerServiceImpl(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public List<answer> getAnswerByQuestionId(String id) {
        List<answer> redisAnswers = (List<answer>) redisTemplate.opsForValue().get(redisKey.answerQueryKey + id);
        if (!ObjectUtils.isEmpty(redisAnswers)) {
            return redisAnswers;
        }
        LambdaQueryWrapper<answer> answerLambdaQueryWrapper = new LambdaQueryWrapper<>();
        answerLambdaQueryWrapper.eq(StringUtils.hasLength(id), answer::getQuestionId, id);
        List<answer> answers = baseMapper.selectList(answerLambdaQueryWrapper);
        redisTemplate.opsForValue().set(redisKey.answerQueryKey + id, answers, StringCommon.TimeOut, TimeUnit.HOURS);
        return answers;
    }

    @Override
    public boolean deleteAnswerByQuestionId(String[] ids) {
        LambdaQueryWrapper<answer> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(answer::getId, Arrays.asList(ids));
        int delete = baseMapper.delete(queryWrapper);
        List<String> idsList = List.of(ids);
        idsList = idsList.stream().map(t -> redisKey.answerQueryKey + t).toList();
        redisTemplate.opsForValue().getOperations().delete(idsList);
        return delete > paramsKey.NUM_ZERO;
    }

    @Override
    public boolean saveAnswer(String id, answer answer) {
        //判断id是否存在
        Integer resultCount = baseMapper.queryQuestionById(id);
        if (ObjectUtils.isEmpty(resultCount)) {
            return false;
        }
        //插入questionId
        answer.setQuestionId(id);
        int insert = baseMapper.insert(answer);
        return insert > paramsKey.NUM_ZERO;
    }
}
