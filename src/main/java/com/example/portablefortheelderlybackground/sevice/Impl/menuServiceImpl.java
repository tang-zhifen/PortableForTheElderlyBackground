package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.menuMapper;
import com.example.portablefortheelderlybackground.pojo.RABC.menu;
import com.example.portablefortheelderlybackground.sevice.menuService;
import org.springframework.stereotype.Service;

@Service
public class menuServiceImpl extends ServiceImpl<menuMapper, menu> implements menuService {
}
