package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.SurveyRelationQuestionMapper;
import com.example.portablefortheelderlybackground.pojo.question.surveyRelationQuestion;
import com.example.portablefortheelderlybackground.sevice.SurveyRelationQuestionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.util.List;

@Slf4j
@Service
@Transactional(timeout = 5)
public class SurveyRelationQuestionServiceImpl extends ServiceImpl<SurveyRelationQuestionMapper, surveyRelationQuestion> implements SurveyRelationQuestionService {


    @Override
    public List<String> getQuestionIdListBySurveyId(String id) {
        //对id进行非空判断
        if (!StringUtils.hasLength(id)) {
            return null;
        }
        List<String> questionIdListBySurveyId = baseMapper.getQuestionIdListBySurveyId(id);
        //对list进行非空判断
        if (ObjectUtils.isEmpty(questionIdListBySurveyId)) {
            return null;
        }
        //返回question的id集合
        return questionIdListBySurveyId;
    }
}
