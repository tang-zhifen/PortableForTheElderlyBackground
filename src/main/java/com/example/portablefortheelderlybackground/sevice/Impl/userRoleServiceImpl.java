package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.userRoleMapper;
import com.example.portablefortheelderlybackground.pojo.RABC.userRole;
import com.example.portablefortheelderlybackground.sevice.userRoleService;
import org.springframework.stereotype.Service;

@Service
public class userRoleServiceImpl extends ServiceImpl<userRoleMapper, userRole> implements userRoleService {
}
