package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.mapper.roleMenuMapper;
import com.example.portablefortheelderlybackground.pojo.RABC.roleMenu;
import com.example.portablefortheelderlybackground.sevice.roleMenuService;
import org.springframework.stereotype.Service;

@Service
public class roleMenuServiceImpl extends ServiceImpl<roleMenuMapper, roleMenu> implements roleMenuService {
}
