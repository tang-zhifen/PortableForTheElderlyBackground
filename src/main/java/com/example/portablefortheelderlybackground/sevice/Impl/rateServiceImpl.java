package com.example.portablefortheelderlybackground.sevice.Impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.portablefortheelderlybackground.common.StringCommon;
import com.example.portablefortheelderlybackground.common.key.paramsKey;
import com.example.portablefortheelderlybackground.common.redis.redisKey;
import com.example.portablefortheelderlybackground.mapper.rateMapper;
import com.example.portablefortheelderlybackground.pojo.rate.rate;
import com.example.portablefortheelderlybackground.sevice.rateService;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.rateVo.rateVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.example.portablefortheelderlybackground.Enum.Message.ReturnMessageEnum.*;

@Slf4j
@Service
@Transactional(timeout = 5)
public class rateServiceImpl extends ServiceImpl<rateMapper, rate> implements rateService {

    private final
    RedisTemplate<String, Object> redisTemplate;

    @Autowired
    public rateServiceImpl(RedisTemplate<String, Object> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public Response<Page<rateVo>> getAllRateVo(String type, Integer pages, Integer pageSize) {
        //查询缓存是否存在
        Page<rateVo> page = (Page<rateVo>) redisTemplate.opsForValue().get(redisKey.rateKey + type);
        if (!ObjectUtils.isEmpty(page)) {
            return Response.status(page, SUCCESS.getCode(), SUCCESS.getMessage());
        }
        LambdaQueryWrapper<rate> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(rate::getType, type);
        Page<rate> ratePage = new Page<>(pages, pageSize);
        baseMapper.selectPage(ratePage, queryWrapper);
        //通过rate构建rateVo对象
        List<rateVo> rateVos = ratePage.getRecords().stream().map(rateVo::new).toList();
        //构建page<rateVo>
        Page<rateVo> rateVoPage = new Page<>();
        //复制page<ratePage>属性
        BeanUtils.copyProperties(ratePage, rateVoPage, "records");
        rateVoPage.setRecords(rateVos);
        //存缓存
        redisTemplate.opsForValue().set(redisKey.rateKey + type, ratePage, StringCommon.TimeOut, TimeUnit.HOURS);
        return Response.status(rateVoPage, SUCCESS.getCode(), SUCCESS.getMessage());
    }

    @Override
    public Response<Boolean> saveRateVo(rateVo vo) {
        rate rate = new rate();
        //忽略id复制
        BeanUtils.copyProperties(vo, rate, "id", "createTime", "updateTime");
        //进行插入
        int insert = baseMapper.insert(rate);
        boolean isSuccess = insert > paramsKey.NUM_ZERO;
        if (isSuccess) {
            redisTemplate.opsForValue().getOperations().delete(redisKey.rateKey + rate.getType());
        } else {
            return Response.status(false, SAVE_ERROR.getCode(), SAVE_ERROR.getMessage());
        }
        return Response.status(true, SAVE_SUCCESS.getCode(), SAVE_SUCCESS.getMessage());
    }

    @Override
    public Response<Boolean> deleteRateVoByIds(String[] ids) {
        //删除对应的redis缓存,这里暂时先不删,对数据一致性要求不高
        List<String> deleteIds = List.of(ids);
        int i = baseMapper.deleteBatchIds(deleteIds);
        if (i > paramsKey.NUM_ZERO) {
            return Response.status(true, DELETE_SUCCESS.getCode(), DELETE_SUCCESS.getMessage());
        }
        return Response.status(false, DELETE_ERROR.getCode(), DELETE_ERROR.getMessage());
    }
}
