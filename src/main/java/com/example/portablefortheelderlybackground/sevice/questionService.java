package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.question.question;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.questionVo.topicVo;

public interface questionService extends IService<question> {
    // 传入page参数,保存查询的数据,并返回
    IPage<topicVo> getAllTopicVo(IPage<topicVo> page, String type);

    topicVo getTopicVoById(String id);

    Response<Boolean> deleteQuestionsBatch(String[] ids);

    Response<topicVo> addQuestion(topicVo topicVo);

    IPage<topicVo> getAllTopicVoById(IPage<topicVo> page, String id);

}
