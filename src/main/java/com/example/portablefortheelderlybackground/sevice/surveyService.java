package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.question.survey;
import com.example.portablefortheelderlybackground.vo.questionVo.surveyVo;


public interface surveyService extends IService<survey> {
    IPage<surveyVo> getSurveyById(String id);

    //todo 保存用户提交的survey,保存的时候添加人数?,先暂时添加人数
    Boolean saveSurveyVoPeoples(surveyVo vo);

    IPage<surveyVo> getSurveyByCurrent();
}
