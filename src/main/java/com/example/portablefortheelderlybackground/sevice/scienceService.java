package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.dto.scienceDto.scienceDto;
import com.example.portablefortheelderlybackground.pojo.science.science;
import com.example.portablefortheelderlybackground.utils.Response;

public interface scienceService extends IService<science> {
    Response<Page<scienceDto>> getAllScienceMessage(Integer pages, Integer pageSize, String type, String title);

    Response<scienceDto> getScienceById(String id);

    Response saveScienceMessage(scienceDto scienceDto);

    Response<Page<scienceDto>> getScienceByTitle(String title, Integer pages, Integer pageSize);
}
