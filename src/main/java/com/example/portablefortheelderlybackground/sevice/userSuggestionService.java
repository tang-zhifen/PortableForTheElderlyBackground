package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.rate.userSuggestion;
import com.example.portablefortheelderlybackground.utils.Response;

public interface userSuggestionService  extends IService<userSuggestion> {
    Response<Boolean> saveUserSuggestion(userSuggestion userSuggestion);
}
