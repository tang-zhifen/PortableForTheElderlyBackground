package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.question.surveyRelationQuestion;

import java.util.List;

public interface SurveyRelationQuestionService extends IService<surveyRelationQuestion> {
    List<String> getQuestionIdListBySurveyId(String id);
}
