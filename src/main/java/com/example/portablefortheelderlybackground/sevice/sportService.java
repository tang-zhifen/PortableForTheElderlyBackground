package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.sport.sport;
import com.example.portablefortheelderlybackground.utils.Response;

public interface sportService extends IService<sport> {
    Response<Page<sport>> getSportList(Integer pages, Integer pageSize, String type);

    Response<sport> getSportById(String id);

    Response<sport> updateSportById(sport updateSport);

    Response saveSport(sport saveSport);

    Response deleteSport(String id);
}
