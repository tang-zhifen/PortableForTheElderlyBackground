package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.welcome.welcome;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.welcomeVo.welcomeVo;

public interface welcomeService extends IService<welcome> {
    Response<welcomeVo> getSwiperByType(String type);

    Response<Boolean> deleteSwiperByIds(String[] ids);
}
