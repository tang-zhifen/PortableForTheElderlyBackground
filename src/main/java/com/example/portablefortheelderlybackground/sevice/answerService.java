package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.question.answer;

import java.util.List;

public interface answerService extends IService<answer> {


    List<answer> getAnswerByQuestionId(String id);

    boolean deleteAnswerByQuestionId(String[] ids);

    boolean saveAnswer(String id, answer answer);
}
