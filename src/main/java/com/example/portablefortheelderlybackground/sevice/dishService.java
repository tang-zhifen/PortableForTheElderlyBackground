package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.dish.dish;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.dishVo.dishVo;

import java.util.List;

public interface dishService extends IService<dish> {
    Response<Page<dish>> getDishList(Integer pages, Integer pageSize, String type);

    Response<dishVo> getDishById(String id);

    Response<dishVo> updateDishById(dish updateDish);

    Response<dishVo> saveDish(dish saveDish);

    Response<dishVo> saveDishBatch(List<dish> saveDishBatch);


    Response<Boolean> deleteDish(String id);
}
