package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.order.order;

public interface orderService extends IService<order> {
    Boolean createOrder(String id);
}
