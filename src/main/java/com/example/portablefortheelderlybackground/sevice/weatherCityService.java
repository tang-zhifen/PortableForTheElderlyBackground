package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.weather.weatherCity;

public interface weatherCityService extends IService<weatherCity> {
    public Boolean getWeatherByWeek(String cityId, String appId, String appSecret);
}
