package com.example.portablefortheelderlybackground.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.portablefortheelderlybackground.pojo.history.lookHistory;
import com.example.portablefortheelderlybackground.utils.Response;
import com.example.portablefortheelderlybackground.vo.historyVo.lookAtScienceViewVo;
import com.example.portablefortheelderlybackground.vo.historyVo.lookAtScienceVo;

import java.util.List;

public interface lookHistoryService extends IService<lookHistory> {
    Response<String> saveScienceHistory(List<lookAtScienceVo> lookAtScienceVo);

    Response<List<lookAtScienceViewVo>> queryScienceHistoryByUserId(String userId, Integer page, Integer pageSize);

    Response<Boolean> deleteScienceHistory(String userId, String[] scienceIds);

    Response<Boolean> deleteScienceByStatus();
}
