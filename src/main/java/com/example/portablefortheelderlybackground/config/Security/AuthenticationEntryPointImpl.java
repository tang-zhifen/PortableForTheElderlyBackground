package com.example.portablefortheelderlybackground.config.Security;

import com.example.portablefortheelderlybackground.pojo.userDetailImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Slf4j
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        //1.身份验证失败时,从上下文对象获取主体
        response.setHeader("unauthenicationError", "身份验证未通过");
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
    }

}
