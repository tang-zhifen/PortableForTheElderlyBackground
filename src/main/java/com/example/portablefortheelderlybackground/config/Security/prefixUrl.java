package com.example.portablefortheelderlybackground.config.Security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author 86187
 * @description: url前缀
 * @date: 2023/7/4 15:38
 */
@Component
public class prefixUrl {
    //生产环境context
    public static String PROD_PREFIX_URL = "";
    //开发环境context
    public static String DEV_PREFIX_URL = "";

    @Value("${server.servlet.contextPath}")
    public void setDevPrefixUrl(String dev_prefix_url) {
        DEV_PREFIX_URL = dev_prefix_url;
    }

    @Value("${server.servlet.contextPath}")
    public void setProdPrefixUrl(String prod_prefix_url) {
        PROD_PREFIX_URL = prod_prefix_url;
    }
}
