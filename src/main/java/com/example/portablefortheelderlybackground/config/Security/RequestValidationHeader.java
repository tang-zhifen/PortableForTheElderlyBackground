package com.example.portablefortheelderlybackground.config.Security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RequestValidationHeader {
    //验证请求头,即token请求头,用于实际请求验证的token
    @Value("${request.header.authentication}")
    public String Authentication;
    //刷新token请求头,即刷新请求头,用于用户刷新token,实现长时间在线功能
    @Value("${request.header.refreshAuthentication}")
    public String refreshAuthentication;
}
