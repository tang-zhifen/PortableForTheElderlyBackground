package com.example.portablefortheelderlybackground.config.Security;

import com.example.portablefortheelderlybackground.pojo.userDetailImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class AccessDeniedExceptionImpl implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        //从security上下文对象获取userName
        response.setHeader("unauthenizationError", "身份未拥有该权限");
        response.setStatus(HttpStatus.FORBIDDEN.value());
    }
}
