package com.example.portablefortheelderlybackground.config.Security;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 86187
 * @description: 允许匿名访问的接口
 * @date: 2023/7/4 12:42
 */
@Component
public class AllowedInterfaces {
    /**
     * @return java.lang.String[]
     * @description: 登录与否均可查看的资源
     * @author 86187
     * @date: 2023/7/15 20:02
     */

    public static String[] getPermitUrl() {
        List<String> list = new ArrayList<>();
        //设置放行路径
        list.add(prefixUrl.DEV_PREFIX_URL + "/answer/query/{id}");
        list.add(prefixUrl.DEV_PREFIX_URL + "/dish/query");
        list.add(prefixUrl.DEV_PREFIX_URL + "/dish/query/{id}");
        list.add(prefixUrl.DEV_PREFIX_URL + "/question/query");
        list.add(prefixUrl.DEV_PREFIX_URL + "/question/query/{id}");
        list.add(prefixUrl.DEV_PREFIX_URL + "/rate/query");
        list.add(prefixUrl.DEV_PREFIX_URL + "/science/query");
        list.add(prefixUrl.DEV_PREFIX_URL + "/science/query/{id}");
        list.add(prefixUrl.DEV_PREFIX_URL + "/science/search");
        list.add(prefixUrl.DEV_PREFIX_URL + "/sport/query");
        list.add(prefixUrl.DEV_PREFIX_URL + "/sport/query/{id}");
        list.add(prefixUrl.DEV_PREFIX_URL + "/survey/query");
        list.add(prefixUrl.DEV_PREFIX_URL + "/survey/query/{id}");
        list.add(prefixUrl.DEV_PREFIX_URL + "/welcome/query");
        list.add(prefixUrl.DEV_PREFIX_URL + "/api/uptoken");
        /* **********   swagger放行    **********/
        list.add(prefixUrl.DEV_PREFIX_URL + "/doc.html/**");
        list.add(prefixUrl.DEV_PREFIX_URL + "/swagger-resources");
        list.add(prefixUrl.DEV_PREFIX_URL + "/webjars/**");
        list.add(prefixUrl.DEV_PREFIX_URL + "/favicon.ico");
        list.add(prefixUrl.DEV_PREFIX_URL + "/v2/api-docs");
        /* **********   刷新token接口开放    **********/
        list.add(prefixUrl.DEV_PREFIX_URL + "/user/refreshToken");
        /* **********   历史记录接口开放    **********/
        list.add(prefixUrl.DEV_PREFIX_URL + "/history/science/query/{id}/{page}");
        String[] allowed = new String[list.size()];
        for (int i = 0; i < allowed.length; i++) {
            allowed[i] = list.get(i);
        }
        return allowed;
    }

    /**
     * @return java.lang.String[]
     * @description: 登录后不可访问的资源
     * @author 86187
     * @date: 2023/7/15 20:03
     */

    public static String[] getAnonymousUrl() {
        return new String[]{"/user/login", "/user/signIn", "/user/phoneSignIn", "/user/phoneLogin"};
    }
}
