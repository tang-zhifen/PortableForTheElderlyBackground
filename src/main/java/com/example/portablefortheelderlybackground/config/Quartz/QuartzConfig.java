package com.example.portablefortheelderlybackground.config.Quartz;

import com.example.portablefortheelderlybackground.config.Quartz.job.ClearScienceHistoryByUserAndTimeJob;
import com.example.portablefortheelderlybackground.config.Quartz.job.CrawlWeatherScheduledTasks;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuartzConfig {
    @Bean
    public JobDetail cleanScienceHistoryJobDetail() {
        return JobBuilder.newJob(ClearScienceHistoryByUserAndTimeJob.class).withDescription("")// 任务描述
                .withIdentity("测试任务")// 任务名称
                .storeDurably()// 每次任务执行后存储
                .build();// 构建
    }

    @Bean
    public JobDetail CrawlWeatherJobDetail() {
        return JobBuilder.newJob(CrawlWeatherScheduledTasks.class).withDescription("")// 任务描述
                .withIdentity("获取天气")// 任务名称
                .storeDurably()// 每次任务执行后存储
                .build();// 构建
    }

    @Bean
    public Trigger cleanScienceHistoryTrigger() {
        //创建触发器
        return TriggerBuilder.newTrigger()
                // 绑定工作任务
                .forJob(cleanScienceHistoryJobDetail())
                // 每隔 5 秒执行一次 job
                .withIdentity("ClearHistory")
                // 每月15号中午12点执行该定时任务
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 12 15 * ?")).build();
    }

    @Bean
    public Trigger CrawlWeatherTrigger() {
        //创建触发器
        return TriggerBuilder.newTrigger()
                // 绑定工作任务
                .forJob(CrawlWeatherJobDetail())
                // 每隔 5 秒执行一次 job
                .withIdentity("CrawWeather")
                // 每周的星期一0点进行定时调度
                .withSchedule(CronScheduleBuilder.cronSchedule("0 0 0 ? * 1")).build();
    }
}
