package com.example.portablefortheelderlybackground.config.Quartz.job;

import com.example.portablefortheelderlybackground.config.weatherAPI.tianqiParams;
import com.example.portablefortheelderlybackground.sevice.weatherCityService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
@Slf4j
public class CrawlWeatherScheduledTasks extends QuartzJobBean {


    private final weatherCityService weatherCityService;
    //导入天气的component
    private final tianqiParams tianqiParams;


    @Autowired
    public CrawlWeatherScheduledTasks(weatherCityService weatherCityService, tianqiParams tianqiParams) {
        this.weatherCityService = weatherCityService;
        this.tianqiParams = tianqiParams;
    }


    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("{}开始执行定时爬取天气任务", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        Boolean flag = weatherCityService.getWeatherByWeek(tianqiParams.getCityId(), tianqiParams.getAppId(), tianqiParams.getAppSecret());
        log.info("{}定时爬取天气任务{}", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), flag ? "成功" : "失败");
    }
}
