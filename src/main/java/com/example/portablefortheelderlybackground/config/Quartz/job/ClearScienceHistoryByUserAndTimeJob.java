package com.example.portablefortheelderlybackground.config.Quartz.job;

import com.example.portablefortheelderlybackground.sevice.lookHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author 86187
 * @description: 定时清理用户观看记录
 * @date: 2023/10/19 23:12
 */
@Component
@Slf4j
public class ClearScienceHistoryByUserAndTimeJob extends QuartzJobBean {
    private final lookHistoryService lookHistoryService;

    public ClearScienceHistoryByUserAndTimeJob(lookHistoryService lookHistoryService) {
        this.lookHistoryService = lookHistoryService;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
        log.info("{}开始执行删除历史记录任务", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        //执行定时任务
        Boolean isSUCCESS = lookHistoryService.deleteScienceByStatus().data;
        log.info("{}执行删除定历史记录时任务{}", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")), isSUCCESS ? "成功" : "失败");
    }
}
