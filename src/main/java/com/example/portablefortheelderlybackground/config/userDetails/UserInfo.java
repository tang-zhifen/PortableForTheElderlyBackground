package com.example.portablefortheelderlybackground.config.userDetails;

import com.example.portablefortheelderlybackground.pojo.userDetailImpl;
import org.springframework.security.core.context.SecurityContextHolder;

public class UserInfo {
    public static userDetailImpl getUserInfo() {
        return (userDetailImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
