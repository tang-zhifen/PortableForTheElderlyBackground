package com.example.portablefortheelderlybackground.config.userDetails;

/**
 * @author 86187
 * @description: 保存单次线程的变量, 用于暂存用户名, 其实也可以使用security的上下文
 * @date: 2023/7/15 16:58
 */

public class AuthThread {
    private static final ThreadLocal<String> thread = new ThreadLocal<>();

    public static String get() {
        return thread.get();
    }

    public static void set(String value) {
        thread.set(value);
    }
}
