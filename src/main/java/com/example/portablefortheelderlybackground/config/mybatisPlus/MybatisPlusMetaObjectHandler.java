package com.example.portablefortheelderlybackground.config.mybatisPlus;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.example.portablefortheelderlybackground.common.key.paramsKey;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author 86187
 * @description: 设置字段的默认填充, createTime, updateTime
 * @date: 2023/7/6 19:15
 */

@Slf4j
@Component
public class MybatisPlusMetaObjectHandler implements MetaObjectHandler {
    //    插入时,对字段进行填充,运用了aop的思想,在执行插入方法之后,sql语句之前进行字段的填充
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("新增自定填充字段");
        this.strictInsertFill(metaObject, "createTime", LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, "isDelete", Integer.class, paramsKey.NOT_DELETE);
    }

    //    更新时,对字段进行填充,运用了aop的思想,在执行更新方法之后,sql语句之前进行字段的填充
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("修改时修改字段");
        this.strictUpdateFill(metaObject, "updateTime", LocalDateTime.class, LocalDateTime.now());
    }
}
