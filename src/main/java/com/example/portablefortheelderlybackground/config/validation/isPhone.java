package com.example.portablefortheelderlybackground.config.validation;

import com.example.portablefortheelderlybackground.config.validation.Validated.isPhoneValidation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @author 86187
 * @description: 自定义手机效验注解
 * @date: 2023/7/5 12:49
 */

@Target(value = {ElementType.FIELD, ElementType.PARAMETER})
@Retention(value = RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {isPhoneValidation.class})//效验的逻辑类
public @interface isPhone {
    String message() default "手机号不规范";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String value() default "手机号验证";
}
