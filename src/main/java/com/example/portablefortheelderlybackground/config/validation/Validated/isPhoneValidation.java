package com.example.portablefortheelderlybackground.config.validation.Validated;

import com.example.portablefortheelderlybackground.config.validation.isPhone;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author 86187
 * @description: 手机号自定义效验处理
 * @date: 2023/7/6 9:40
 */


public class isPhoneValidation implements ConstraintValidator<isPhone, String> {
    @Override
    public void initialize(isPhone constraintAnnotation) {
        String value = constraintAnnotation.value();
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (!StringUtils.hasLength(s))
            return false;
        //正则表达式匹配
        return s.matches("^(?:(?:\\+|00)86)?1[3-9]\\d{9}$");
    }
}
