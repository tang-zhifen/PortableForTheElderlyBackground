package com.example.portablefortheelderlybackground.config.Tencent.SMS;

import com.example.portablefortheelderlybackground.common.StringCommon;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * @author tzf
 * @description: 腾讯云的ApiId
 * @date: 2023/7/4 11:01
 */
@Component
public class ApiId {
    // 密钥id
    public static String secretId;
    // 密钥值
    public static String secretKey;
    // 短信应用id
    public static String sdkAppId;
    // 短信模板签证名
    public static String signName;
    // 模板id
    public static String templateId;

    @Value("${sms.module.Api.secretId}")
    public void setSecretId(String secretId) {
        ApiId.secretId = secretId;
    }

    @Value("${sms.module.Api.secretKey}")
    public void setSecretKey(String secretKey) {
        ApiId.secretKey = secretKey;
    }

    @Value("${sms.module.Api.sdkAppId}")
    public void setSdkAppId(String sdkAppId) {
        ApiId.sdkAppId = sdkAppId;
    }

    @Value("${sms.module.Api.signName}")
    public void setSignName(String signName) {
        ApiId.signName = signName;
    }

    @Value("${sms.module.Api.templateId}")
    public void setTemplateId(String templateId) {
        ApiId.templateId = templateId;
    }

    // 模板参数
    public static String[] generateTemplateParamSet(String code) {
        return new String[]{code, String.valueOf(StringCommon.SMSCodeTimeOut)};
    }

    // 构建phoneNumSet
    public static String[] generatePhoneNumSet(String... phone) {
        String[] res = new String[phone.length];
        int index = 0;
        for (String s : phone) {
            if (StringUtils.hasLength(s)) {
                res[index++] = s;
            }
        }
        return res;
    }
}
