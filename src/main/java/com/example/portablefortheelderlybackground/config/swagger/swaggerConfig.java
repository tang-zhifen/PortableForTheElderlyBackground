package com.example.portablefortheelderlybackground.config.swagger;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableKnife4j
@EnableSwagger2
public class swaggerConfig {

    @Value("${swaggerConfig.title}")
    private String title;
    @Value("${swaggerConfig.contact.name}")
    private String name;
    @Value("${swaggerConfig.contact.url}")
    private String url;
    @Value("${swaggerConfig.contact.email}")
    private String email;
    @Value("${swaggerConfig.description}")
    private String description;
    @Value("${swaggerConfig.version}")
    private String version;

    @Bean
    public Docket docket() {
//        创建docket对象,其中DocumentationType.SWAGGER_2是swagger2的配置
        Docket docket = new Docket(DocumentationType.SWAGGER_2);
//        构建者模式
        ApiInfo apiInfo = new ApiInfoBuilder().title(title).//设置标题
                contact(new Contact(name, url, email))//设置联系方式
                .description(description)//设置描述信息
                .version(version)//设置版本号
                .build();//构建
        docket.apiInfo(apiInfo);
//        设置扫描包
        docket = docket.select().apis(RequestHandlerSelectors.basePackage("com.example.portablefortheelderlybackground")).build();//重新构建
        return docket;
    }
}
