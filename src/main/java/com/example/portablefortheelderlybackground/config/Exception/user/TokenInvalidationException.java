package com.example.portablefortheelderlybackground.config.Exception.user;

import java.io.Serial;

public class TokenInvalidationException extends RuntimeException {
    @Serial
    private static final long serialVersionUID = -4032409081295836046L;

    public TokenInvalidationException(String message) {
        super(message);
    }
}
