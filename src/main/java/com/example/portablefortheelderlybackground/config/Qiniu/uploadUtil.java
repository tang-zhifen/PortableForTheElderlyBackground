package com.example.portablefortheelderlybackground.config.Qiniu;

import com.qiniu.util.Auth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component//交由spring管理
public class uploadUtil {
    static String accessKey;
    static String secretKey;
    static String bucket;

    @Value("${qiniu.accessKey}")
    public void setAccessKey(String accessKeys) {
        accessKey = accessKeys;
    }

    @Value("${qiniu.secretKey}")
    public void setSecretKey(String secretKeys) {
        secretKey = secretKeys;
    }

    @Value("${qiniu.bucket}")
    public void setBucket(String buckets) {
        bucket = buckets;
    }

    //供自己使用的token
    public String uploadFile() {
        Auth auth = Auth.create(accessKey, secretKey);
        return auth.uploadToken(bucket);
    }

    //供其他人使用的接口,七牛云token
    public String uploadFileSelfToken(String accessKey, String secretKey, String bucket) {
        Auth auth = Auth.create(accessKey, secretKey);
        return auth.uploadToken(bucket);
    }
}
