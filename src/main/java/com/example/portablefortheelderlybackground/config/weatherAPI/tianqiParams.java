package com.example.portablefortheelderlybackground.config.weatherAPI;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "weather")
@Data
public class tianqiParams {
    private String appId;
    private String appSecret;
    private String cityId;
}
