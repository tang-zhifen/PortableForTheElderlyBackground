FROM openjdk:17
# 暴露端口
EXPOSE 8082
# 声明env
ENV tag 1.2
# 工作目录
WORKDIR /app
ADD PortableForTheElderlyBackground-$tag-SNAPSHOT.jar /app/back.jar
# 执行命令
ENTRYPOINT [ "java","-jar", "/app/back.jar"]