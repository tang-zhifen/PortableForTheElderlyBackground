## `便捷老人服务后端`

### `开发意图`

>意在开发一款微信小程序，用于向老人和相应人群的进行科普，和知识宣传，例如，如何乘坐高铁等流程，以及进行饮食方面的推荐，目前功能较少。局限性较大，可能距离真正投入使用还差的很多。如果你有这方面的建议，欢迎提出



### `技术选型`

>* 服务搭建:`springBoot`,`mybatis-plus`
>* 数据库:`MYSQL`
>* 缓存使用:`Redis`
>* 安全效验:`springSecurity`,`JWT`
>* 参数效验:`springValidation`
>* 接口文档:`swagger2`,`swagger-ui`,`knif4j`
>* 短信服务:`腾讯云`
>* *OSS*对象服务:`qiniu-七牛云`
>* 其他工具库:`hutools`
>
>该项目的`springBoot`版本<font color="yellow">2.5.6</font>,`maven`编译和运行的`jdk`版本是<font color="yellow">17</font>



### `构建Docker镜像`

>支持`docker`制作镜像,可通过修改`Dockerfile`中的配置修改构建参数
>
>通过`docker`构建镜像可省去像`jdk`的等环境配置

```dockerfile
# jdk环境,这里是从docker镜像拉取jdk17,构建镜像
FROM openjdk:17 
# 暴露端口
EXPOSE 8082
# 声明env 
ENV tag 1.2 # 项目版本号
# 工作目录
WORKDIR /app
ADD PortableForTheElderlyBackground-$tag-SNAPSHOT.jar /app/back.jar
# 执行命令
ENTRYPOINT [ "java","-jar", "/app/back.jar"]
```



### `配置文件`

>项目中所需要像的第三方的参数,都写在的`*-prod.yml`的配置文件里,可通过修改配置文件里的里参数,以启动该项目

